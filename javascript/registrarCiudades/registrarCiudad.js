/**
 * Created by Mannlex21 on 14/03/17.
 */
var contraCorrecta="dtapp2017#";
$(document).on('ready',function(){
    $(document).on('click','#registroCiudad',function(){
        registro($('#inputCiudad').val(),$('#selectEstado').val(),$('#inputPass').val());
        return false;
    });
});
function registro(ciudad,estado,contra) {
    if(contra==contraCorrecta){
        var div = document.createElement('div');
        div.setAttribute('class','divPass');

        var ciudadSinAcentos = quitaAcentos(ciudad);
        var estadoSinAcentos = quitaAcentos(estado);

        // var keyCiudad = firebase.database().ref('detodoap/ciudades').child('ciudadKey').push().key;
        var updates = {};
        var registro = {
            'ciudad':ciudad,
            'estado':estado
        };
        updates['ciudades/'+estadoSinAcentos.toLowerCase()+'/'+ciudadSinAcentos.toLocaleLowerCase()+'/'] = registro;

        var onComplete = function(error) {
            if (error) {
                console.log('Synchronization failed');
            } else {
                console.log('Synchronization succeeded');
                location.reload();
            }
        };
        firebase.database().ref().update(updates,onComplete);
    }else{
        alert("La contraseña es incorrecta");
    }

}
function quitaAcentos(str){
    for (var i=0;i<str.length;i++){
        //Sustituye "á é í ó ú"
        if (str.charAt(i)=="á") str = str.replace(/á/,"a");
        if (str.charAt(i)=="é") str = str.replace(/é/,"e");
        if (str.charAt(i)=="í") str = str.replace(/í/,"i");
        if (str.charAt(i)=="ó") str = str.replace(/ó/,"o");
        if (str.charAt(i)=="ú") str = str.replace(/ú/,"u");
        if (str.charAt(i)=="ñ") str = str.replace(/ñ/,"n");
        if (str.charAt(i)==" ") str = str.replace(" ","");
    }
    return str;
}

