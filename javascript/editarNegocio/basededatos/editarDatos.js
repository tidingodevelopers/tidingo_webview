var banIrCiu;
var latitudMap;
var longitudMap;
var latitudAct;
var latitudActual;
var longituActual;
var mapitaCreado=0;
var ciudadCh;
var estadoCh;
var horariocreado=0;
function mostrarDatos(){
	//console.log("entra");
	ultimaPginaV=localStorage.getItem("ultimaPag");
	if(ultimaPginaV!="ciudades"){
		localStorage.setItem("ciudadEdit", "0");
		localStorage.setItem("estadoEdit", "0");
	}

	urlPag=window.location.href;
  	nombreUserPerfil=urlPag.split("/editarNegocio.html?").pop();
  	ciudadCh=localStorage.getItem("ciudadEdit");
	estadoCh=localStorage.getItem("estadoEdit");

	if(ciudadCh!="0"){
		var ciudadPoner = document.getElementById("estadociduad");
		ciudadPoner.innerHTML=estadoCh+"-"+ciudadCh;
	}

	var user = firebase.auth().currentUser;
	starCountRef = firebase.database().ref('users/'+user.uid);
	starCountRef.on('value', function(snapshot) {
	  //console.log(snapshot.val());
	datos=snapshot.val();
	if(datos.nombre!=undefined){
		  var nombrePoner = document.getElementById("editarNombreNegocio");
		  nombrePoner.setAttribute("value",datos.nombre);
	}
	if(ciudadCh=="0"){
		if(datos.municipio!=undefined && datos.estado!=undefined){
			ciudadCh=datos.municipio;
			estadoCh=datos.estado;
			var ciudadPoner = document.getElementById("estadociduad");
			ciudadPoner.innerHTML=datos.estado+"-"+datos.municipio;
		}
	}

	$.ajax({
	    type: "GET",
	    url: 'https:maps.googleapis.com/maps/api/geocode/json?address='+ciudadCh+','+estadoCh+'&key=AIzaSyDBXtlD3uXIHkTMyH-5r2GliSOxAR-ZSaM',
	    dataType: "json"
	}).success(function(data){
	    datos=data;
	    //console.log(data.results[0].geometry.location)
	    latitudAct=data.results[0].geometry.location.lat;
	    longitudAct=data.results[0].geometry.location.lng;
	});

	if(datos.descripcion!=undefined){
		  var descripPon = document.getElementById("editarDescripcion");
		  descripPon.setAttribute("value",datos.descripcion);
	}
	if(datos.pagweb!=undefined){
		  var pagPon = document.getElementById("editarPaginaWeb");
		  pagPon.setAttribute("value",datos.pagweb);
	}
	if(datos.correo!=undefined){
		  var correoPon = document.getElementById("editarCorreoNegocio");
		  correoPon.setAttribute("value",datos.correo);
	}
	if(datos.facebook!=undefined){  
		  var facePon = document.getElementById("editarFacebookNegocio");
		  facePon.setAttribute("value",datos.facebook);
	}
	if(datos.telefono!=undefined){
		  var editPon = document.getElementById("editarTelefonoNegocio");
		  editPon.setAttribute("value",datos.telefono);
	}
	if(datos.telefono2!=undefined){
		  var editPon2 = document.getElementById("editarTelefono2Negocio");
		  editPon2.setAttribute("value",datos.telefono2);
	}
	if(datos.telefono3!=undefined){
		  var editPon3 = document.getElementById("editarTelefono3Negocio");
		  editPon3.setAttribute("value",datos.telefono3);
	}
	if(datos.calle!=undefined){
		  var callePon = document.getElementById("editarCalleNegocio");
		  callePon.setAttribute("value",datos.calle);
	}
	if(datos.palabrasc!=undefined){	  
		  var palabrasCL = document.getElementById("editarPalabrasClave");
		  palabrasCL.value=datos.palabrasc;
	}
	if(datos.horarios){
		if(horariocreado==0){
			if(datos.horarios.horaLunes!=undefined){
				idHorario="horaLunes"
				datacheck=datos.horarios.horaLunes;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaMartes!=undefined){
				idHorario="horaMartes"
				datacheck=datos.horarios.horaMartes;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaMiercoles!=undefined){
				idHorario="horaMiercoles"
				datacheck=datos.horarios.horaMiercoles;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaJueves!=undefined){
				idHorario="horaJueves"
				datacheck=datos.horarios.horaJueves;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaViernes!=undefined){
				idHorario="horaViernes"
				datacheck=datos.horarios.horaViernes;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaSabado!=undefined){
				idHorario="horaSabado"
				datacheck=datos.horarios.horaSabado;
				ponerPalomita(idHorario, datacheck);
			}
			if(datos.horarios.horaDomingo!=undefined){
				idHorario="horaDomingo"
				datacheck=datos.horarios.horaDomingo;
				ponerPalomita(idHorario, datacheck);
			}
		}
		if(datos.horarios.hora1Lu!=undefined){	  
			  var hora1Lun = document.getElementById("hora1Lunes");
			  hora1Lun.value=datos.horarios.hora1Lu;
		}
		if(datos.horarios.hora2Lu!=undefined){	  
			  var hora2Lun = document.getElementById("hora2Lunes");
			  hora2Lun.value=datos.horarios.hora2Lu;
		}
		if(datos.horarios.hora1Ma!=undefined){	  
			  var hora1Mar = document.getElementById("hora1Martes");
			  hora1Mar.value=datos.horarios.hora1Ma;
		}
		if(datos.horarios.hora2Ma!=undefined){	  
			  var hora2Mar = document.getElementById("hora2Martes");
			  hora2Mar.value=datos.horarios.hora2Ma;
		}
		if(datos.horarios.hora1Mi!=undefined){	  
			  var hora1Mie = document.getElementById("hora1Miercoles");
			  hora1Mie.value=datos.horarios.hora1Mi;
		}
		if(datos.horarios.hora2Mi!=undefined){	  
			  var hora2Mie = document.getElementById("hora2Miercoles");
			  hora2Mie.value=datos.horarios.hora2Mi;
		}
		if(datos.horarios.hora1Ju!=undefined){	  
			  var hora1Jue = document.getElementById("hora1Jueves");
			  hora1Jue.value=datos.horarios.hora1Ju;
		}
		if(datos.horarios.hora2Ju!=undefined){	  
			  var hora2Jue = document.getElementById("hora2Jueves");
			  hora2Jue.value=datos.horarios.hora2Ju;
		}
		if(datos.horarios.hora1Vi!=undefined){	  
			  var hora1Vie = document.getElementById("hora1Viernes");
			  hora1Vie.value=datos.horarios.hora1Vi;
		}
		if(datos.horarios.hora2Vi!=undefined){	  
			  var hora2Vie = document.getElementById("hora2Viernes");
			  hora2Vie.value=datos.horarios.hora2Vi;
		}
		if(datos.horarios.hora1Sa!=undefined){	  
			  var hora1Sab = document.getElementById("hora1Sabado");
			  hora1Sab.value=datos.horarios.hora1Sa;
		}
		if(datos.horarios.hora2Sa!=undefined){	  
			  var hora2Sab = document.getElementById("hora2Sabado");
			  hora2Sab.value=datos.horarios.hora2Sa;
		}
		if(datos.horarios.hora1Do!=undefined){	  
			  var hora1Dom = document.getElementById("hora1Domingo");
			  hora1Dom.value=datos.horarios.hora1Do;
		}
		if(datos.horarios.hora2Do!=undefined){	  
			  var hora2Dom = document.getElementById("hora2Domingo");
			  hora2Dom.value=datos.horarios.hora2Do;
		}
	}
	if(datos.descuento!=undefined){	  
		  var descuentoNP = document.getElementById("editarDescuentoNegocio");
		  descuentoNP.value=datos.descuento;
	}
	if(datos.longitudN!=undefined && datos.longitudN!="" && datos.latitudN!=undefined && datos.latitudN!=""){	 
			longituActual= datos.longitudN;
			latitudActual= datos.latitudN; 
			if(mapitaCreado==0){
	        	crearMapaEditar();
	    	}
		  //var longitudNP = document.getElementById("editarLongitudNegocio");
		  //longitudNP.value=datos.longitudN;
	}else{
		$('#contenedormapita').css('display', "none");
		$('#sobreponerMapa').css('display', "none");
	}
	if(datos.imgPerfil!=undefined){	
		if(datos.imgPerfil.img500!=undefined){
			if(datos.imgPerfil.img500.url!=undefined){	  
				  var imgPer = document.getElementById("perfilImg");
				  imgPer.setAttribute("src", datos.imgPerfil.img500.url);
			}
		}
	}
	if(datos.imgPortada!=undefined){ 
		if(datos.imgPortada.img500!=undefined){
			if(datos.imgPortada.img500.url!=undefined){  
				  var imgPort = document.getElementById("principalImg");
				  imgPort.setAttribute("src", datos.imgPortada.img500.url);
			}
		}
	}
	paginaCargada();
	});
}

function subirDatos(){
	con = 0;
	var user = firebase.auth().currentUser;
	var nombreNegocio = $("#editarNombreNegocio").val();
	var descripcion = $("#editarDescripcion").val();
	var calleNeg = $("#editarCalleNegocio").val();
	nombreNegocio=obtenerPrimeraMayuscula(nombreNegocio);
	descripcion=obtenerPrimeraMayuscula(descripcion);
	calleNeg=obtenerPrimeraMayuscula(calleNeg);


	var telefonoNeg = $("#editarTelefonoNegocio").val();
	var telefonoNeg2 = $("#editarTelefono2Negocio").val();
	var telefonoNeg3 = $("#editarTelefono3Negocio").val();
	var paginaWeb = $("#editarPaginaWeb").val();
	var correNeg = $("#editarCorreoNegocio").val();
	var facebookNeg = $("#editarFacebookNegocio").val();
	
	var descuentoNeg = $("#editarDescuentoNegocio").val();
	var palabrasClave = $("#editarPalabrasClave").val();
	var latitudNeg = $("#editarLatitudNegocio").val();
	var longitudNeg = $("#editarLongitudNegocio").val();

	var hora1Lunes = $("#hora1Lunes").val();
	var hora2Lunes = $("#hora2Lunes").val();

	var hora1Martes = $("#hora1Martes").val();
	var hora2Martes = $("#hora2Martes").val();

	var hora1Miercoles = $("#hora1Miercoles").val();
	var hora2Miercoles = $("#hora2Miercoles").val();

	var hora1Jueves = $("#hora1Jueves").val();
	var hora2Jueves = $("#hora2Jueves").val();

	var hora1Viernes = $("#hora1Viernes").val();
	var hora2Viernes = $("#hora2Viernes").val();

	var hora1Sabado = $("#hora1Sabado").val();
	var hora2Sabado = $("#hora2Sabado").val();

	var hora1Domingo = $("#hora1Domingo").val();
	var hora2Domingo = $("#hora2Domingo").val();

	checkLunes = $("#horaLunes").attr('data-check');
	checkMartes = $("#horaMartes").attr('data-check');
	checkMiercoles = $("#horaMiercoles").attr('data-check');
	checkJueves = $("#horaJueves").attr('data-check');
	checkViernes = $("#horaViernes").attr('data-check');
	checkSabado = $("#horaSabado").attr('data-check');
	checkDomingo = $("#horaDomingo").attr('data-check');
    var insertarGeofirebase = false;
	if(latitudActual==undefined){
		latitudActual="";
		longituActual="";
	}else{
		insertarGeofirebase = true;
	}
	/*for(var j=0; j<palabrasClave.length; j++) {
	    palabraCompara = palabrasClave.charAt(j);
	    if(palabraCompara==" "){
	    	con=con+1;
	    }
	}

	if(con>9){
		alert("Solo se permiten 10 palabras clave");
		return false;
	}

	console.log("entra");
	*/
	updates={
		nombre : nombreNegocio,
		descripcion : descripcion,
		calle : calleNeg,
		telefono : telefonoNeg,
		telefono2 : telefonoNeg2,
		telefono3 : telefonoNeg3,
		pagweb : paginaWeb,
		correo : correNeg,
		facebook : facebookNeg,
		descuento : descuentoNeg,
		palabrasc : palabrasClave,
		latitudN: latitudActual,
		longitudN: longituActual,
		estado: estadoCh,
		municipio: ciudadCh
	};

	updatesHor={
		hora1Lu: hora1Lunes,
		hora2Lu: hora2Lunes,
		hora1Ma: hora1Martes,
		hora2Ma: hora2Martes,
		hora1Mi: hora1Miercoles,
		hora2Mi: hora2Miercoles,
		hora1Ju: hora1Jueves,
		hora2Ju: hora2Jueves,
		hora1Vi: hora1Viernes,
		hora2Vi: hora2Viernes,
		hora1Sa: hora1Sabado,
		hora2Sa: hora2Sabado,
		hora1Do: hora1Domingo,
		hora2Do: hora2Domingo,
		horaLunes: checkLunes,
		horaMartes: checkMartes,
		horaMiercoles: checkMiercoles,
		horaJueves: checkJueves,
		horaViernes: checkViernes,
		horaSabado: checkSabado,
		horaDomingo: checkDomingo
	}

	////////////////////////I  N  S  E  R  T  A  R       G  E  O  F  I  R  E  B  A  S  E  ////////////////////////////////
  	if(insertarGeofirebase){
  		console.log(latitudActual,longituActual)
		// Create a Firebase reference where GeoFire will store its information
		var firebaseRef = firebase.database().ref().child("users_location");
		// Create a GeoFire index
		var geoFire = new GeoFire(firebaseRef);
  		geoFire.set(user.uid, [latitudActual,longituActual]).then(function() {
		  console.log("Provided key has been added to GeoFire");
		}, function(error) {
		  console.log("Error: " + error);
		});
  	}


	firebase.database().ref().child("users").child(user.uid).update(updates).then(function(){
		firebase.database().ref().child("users").child(user.uid).child("horarios").update(updatesHor).then(function(){
			cargandoPaginas();
			if(banIrCiu==2){
				location.href='imagenes.html?'+nombreUserPerfil;
			}else if(banIrCiu==1){
				localStorage.setItem("ultimaPag","editarNeg");
				location.href='ciudades.html?'+nombreUserPerfil;
			}else{
	    		location.href="negocio.html?"+nombreUserPerfil;
			}

	  	});

  	});

  	
}

function subirPerfil(){
	var user = firebase.auth().currentUser;
                userId = user.uid;
	uidNeg = userId;
    nuevo = Android.getPerfil(uidNeg);
    //console.log(uidNeg)
}

function subirPortada(){
	var user = firebase.auth().currentUser;
                userId = user.uid;
	uidNeg = userId;
    nuevo2 = Android.getPortada(uidNeg);
    //console.log(uidNeg)
}

function obtenerPrimeraMayuscula(str){
	cantidad=str.length;
	Mayus=str.substr(0,1).toUpperCase();
    str=Mayus+str.substr(1,cantidad); 
    return str; 
}

function ponerPalomita(idHorario, datacheck){
	horariocreado=1;
	var horarioIn = document.getElementById(idHorario);
	if(datacheck=="1"){
		horarioIn.setAttribute('data-check', '1');
		var imagenAde = document.createElement('img');
		imagenAde.setAttribute('id', 'check'+idHorario);
		imagenAde.setAttribute('class', 'palomitaCuadro'); 
		imagenAde.setAttribute('src', 'objetos/negocios/checkedNegra.png');//Esta imagen sale de la D.B.
		horarioIn.appendChild(imagenAde);
	}else{
		horarioIn.setAttribute('data-check', '0');
		$('#check'+idHorario).remove();
	}
}