function crearImagenes(){
	num=0;
	num2=0;
	nume=0;
	var user = firebase.auth().currentUser;
	var divFotos = document.getElementById('imagenesCompletas');
	var todasImg = document.createElement('div');
	todasImg.setAttribute('id',"todasImg");
	divFotos.appendChild(todasImg);
	fotosQ = firebase.database().ref('users/'+user.uid+'/fotosImg/img500');
	fotosQ.once('value', function(snapshot) {
	  //console.log(snapshot.val());
		datos=snapshot.val();
		//console.log(snapshot.key);

		for( neg in datos){
			if(num==0){
				num2=num2+1;
				var divTresImagenes = document.createElement('div');
				divTresImagenes.setAttribute('id', 'tresImages'+num2);
				divTresImagenes.setAttribute('class', 'tresImagenes quincePixeles');
				todasImg.appendChild(divTresImagenes);
			}else if(num%3==0){
				num2=num2+1;
				var divTresImagenes = document.createElement('div');
				divTresImagenes.setAttribute('id', 'tresImages'+num2);
				divTresImagenes.setAttribute('class', 'tresImagenes quincePixeles');
				todasImg.appendChild(divTresImagenes);
			}

			divTresImagenes = document.getElementById('tresImages'+num2);

			var figureFoto = document.createElement('figure');
			figureFoto.setAttribute('id', 'figureFoto'+num);
			figureFoto.setAttribute('class', 'figureFoto quincePixeles');
			figureFoto.setAttribute('data-num', num);
			figureFoto.setAttribute('data-title', datos[neg].nombre);
			figureFoto.setAttribute('data-principal', neg);

			var divAdelante = document.createElement('div');
			divAdelante.setAttribute('class', 'figureFotoDelante quincePixeles');
			divAdelante.setAttribute('id', 'divadelante'+num);
			figureFoto.appendChild(divAdelante);

			var imagenFoto = document.createElement('img'); 
			imagenFoto.setAttribute('src', datos[neg].url);//Esta imagen sale de la D.B.
			figureFoto.appendChild(imagenFoto);
			divTresImagenes.appendChild(figureFoto);

			var imagenAde = document.createElement('img');
			imagenAde.setAttribute('class', 'palomitaCuadro'); 
			imagenAde.setAttribute('src', 'objetos/negocios/checked.png');//Esta imagen sale de la D.B.
			divAdelante.appendChild(imagenAde);

			num=num+1;
		}
		alturasImagenes();
		cargado();
		paginaCargada();

		fotosQ2 = firebase.database().ref('users/'+user.uid+'/fotosImg/img1024');
		fotosQ2.once('value', function(snapshot) {
		  //console.log(snapshot.val());
			datos=snapshot.val();
			//console.log(snapshot.key);

			for( neg in datos){

				figureFoto2 = document.getElementById('figureFoto'+nume);
				figureFoto2.setAttribute('data-title2', datos[neg].nombre);
				figureFoto2.setAttribute('data-principal2', neg);

				nume=nume+1;
			}

		});
	});

	

	/*
	fotosQ.on('child_changed', function(data) {
	  setCommentValues(postElement, data.key, data.val().text, data.val().author);
	});

	fotosQ.on('child_removed', function(data) {
	  deleteComment(postElement, data.key);
	});
	*/
}

function eliminarImagenes(){
	$('#todasImg').remove();
}

function borrarImg(){
	
    var figurePublicar = document.createElement('figure'); 
	figurePublicar.setAttribute('id', 'borrarIm');
	figurePublicar.setAttribute('class', 'quincePixeles');

	var publicar = document.createElement('img'); 
	publicar.setAttribute('src', 'objetos/negocios/trashcan.png');
	figurePublicar.appendChild(publicar);

	
	//var menu = document.getElementById('menu');
	document.body.appendChild(figurePublicar);
}

function publicar(){
	
    var figurePublicar = document.createElement('figure'); 
	figurePublicar.setAttribute('id', 'publicar');
	figurePublicar.setAttribute('class', 'quincePixeles');

	var publicar = document.createElement('img'); 
	publicar.setAttribute('src', 'objetos/generales/subir.png');
	figurePublicar.appendChild(publicar);

	
	//var menu = document.getElementById('menu');
	document.body.appendChild(figurePublicar);
}

function crearDialogoSiNo(Frase){
	ban1=5;
	bandera1(ban1);
	var divFotos = document.getElementById('imagenesCompletas');
	
	var cubrePantallaSiNo = document.createElement('div');
	cubrePantallaSiNo.setAttribute('id', 'cubrePantallaSiNo');

	var cuadroCentradoSiNo = document.createElement('div');
	cuadroCentradoSiNo.setAttribute('id', 'cuadroCentradoSiNo');
	cuadroCentradoSiNo.setAttribute('class', 'quincePixeles');
	document.body.insertBefore(cubrePantallaSiNo, divFotos);
	document.body.insertBefore(cuadroCentradoSiNo, divFotos);

	var titulo = document.createElement('p');
	titulo.setAttribute('id', 'tituloSiNo');
	titulo.setAttribute('class', 'cuarentaYCincoPixeles');
	var textoTitulo = document.createTextNode(Frase);
	titulo.appendChild(textoTitulo);
	cuadroCentradoSiNo.appendChild(titulo);
	$(titulo).css('padding-top', $(document).width() / 13);
	$(titulo).css('padding-bottom', $(document).width() / 13);

	var parrafo = document.createElement('p');
	parrafo.setAttribute('id', 'textoSi');
	parrafo.setAttribute('class', 'textoSiNo cuarentaYCincoPixeles');
	var textoNotificaciones = document.createTextNode("Si");
	parrafo.appendChild(textoNotificaciones);
	cuadroCentradoSiNo.appendChild(parrafo);
	$(parrafo).css('padding-top', $(document).width() / 20);
	$(parrafo).css('padding-bottom', $(document).width() / 20);

	var parrafo2 = document.createElement('p');
	parrafo2.setAttribute('id', 'textoNo');
	parrafo2.setAttribute('class', 'textoSiNo cuarentaYCincoPixeles');
	var textoNotificaciones2 = document.createTextNode("No");
	parrafo2.appendChild(textoNotificaciones2);
	cuadroCentradoSiNo.appendChild(parrafo2);
	$(parrafo2).css('padding-top', $(document).width() / 20);
	$(parrafo2).css('padding-bottom', $(document).width() / 20);

	cargado();		

	$(cuadroCentradoSiNo).css('top', ($(window).height() - $(cuadroCentradoSiNo).height()) / 2);

}

$(window).resize(function(){
	$('#cuadroCentradoSiNo').css('top', ($(window).height() - $('#cuadroCentradoSiNo').height()) / 2);
	$('#tituloSiNo').css('padding-top', $(document).width() / 13);
	$('#tituloSiNo').css('padding-bottom', $(document).width() / 13);
	$('.textoSiNo').css('padding-top', $(document).width() / 20);
	$('.textoSiNo').css('padding-bottom', $(document).width() / 20);
});

function salirDialogoSiNo(){
	ban1=0;
	bandera1(ban1);
	document.body.removeChild(cubrePantallaSiNo);
	document.body.removeChild(cuadroCentradoSiNo);
}