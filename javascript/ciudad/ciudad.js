/**
 * Created by Mannlex21 on 15/03/17.
 */
datos = firebase.database().ref('ciudades').orderByKey();
datos.once("value").then(function(snapshot) {
    var snap = snapshot.val();
    var numEstados=Object.keys(snap).length;
    var estadosAcomodados=new Array();

    for(var i=0;i<numEstados;i++){
        estadosAcomodados[i]=Object.keys(snap)[i];
    }
    estadosAcomodados=estadosAcomodados.sort();

    var listaCiudades = document.getElementById('listaCiudades');
    for(var i=0;i<numEstados;i++){
        var keyEstado= estadosAcomodados[i];
        var numCiudades=Object.keys(snap[keyEstado]).length;
        for (var x=0;x<numCiudades;x++){
            var keyCiudad = Object.keys(snap[keyEstado])[x];
            var textoEstado =snap[keyEstado][keyCiudad]["estado"];
            var textoCiudad=snap[keyEstado][keyCiudad]["ciudad"];
            console.log(textoEstado);
            if(x==0){
                var estadoTexto = document.createElement("p");
                estadoTexto.setAttribute("class","cincuentaPixeles estado");
                estadoTexto.appendChild(document.createTextNode(textoEstado));
                listaCiudades.appendChild(estadoTexto);
            }
            var ciudadTexto = document.createElement("p");
            ciudadTexto.setAttribute("class","cincuentaPixeles ciudad");
            ciudadTexto.setAttribute("data-estado",textoEstado);
            ciudadTexto.setAttribute("data-municipio",textoCiudad);
            ciudadTexto.setAttribute("id",keyEstado+"-"+keyCiudad);
            ciudadTexto.appendChild(document.createTextNode(textoCiudad));
            listaCiudades.appendChild(ciudadTexto);
        }
    }
    cargado();
});