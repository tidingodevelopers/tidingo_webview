banderaHora=0;
var angle = 0;
var facebookNeg;
var webNeg;
$(document).on('ready',function(){
	localStorage.setItem("ultimaPag","negocio");
	miNegocio();
	alturas();
	//haciendoLog();
	$(document).on('keydown','#busquedaNegocio',function(e){
		if(e.keyCode==13){

			e.preventDefault();
			cargandoPaginas();
			enviarVar();
		}
		
	});

	$(document).on('click','#telefono1Call',function(){
		if(llamada!=undefined){
			var ua = navigator.userAgent.toLowerCase();
			var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
			if(isAndroid) {
				lamNu=1;
				llamarTel(lamNu);
			}else{
				
			}
		}else{
			notificaciones('Tidingo', 'Telefono indefinido');
		}
	});

	$(document).on('click','#telefono2Call',function(){
		if(llamada!=undefined){
			var ua = navigator.userAgent.toLowerCase();
			var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
			if(isAndroid) {
				lamNu=2;
				llamarTel(lamNu);
			}else{
				
			}
		}else{
			notificaciones('Tidingo', 'Telefono indefinido');
		}
	});

	$(document).on('click','#telefono3Call',function(){
		if(llamada!=undefined){
			var ua = navigator.userAgent.toLowerCase();
			var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
			if(isAndroid) {
				lamNu=3;
				llamarTel(lamNu);
			}else{
				
			}
		}else{
			notificaciones('Tidingo', 'Telefono indefinido');
		}
	});

	$(document).on('click','#webIr',function(){
		
		if(webNeg!=undefined){
			esface="algo";
			notificaciones('Cargando', 'Por favor espere...');
			webNegReal=webNeg.split("http://").pop();
			webNegReal=webNeg.split("https://").pop();
			location.href="http://"+webNegReal;
		}else{
			
			notificaciones('Tidingo', 'Web indefinido');
		}
	});

	$(document).on('click','#botonFacebook',function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			if(facebookNeg!=undefined){
				esface="algo";
				facebookReal=facebookNeg.split("facebook.com/").pop();
			}else{
				
				notificaciones('Tidingo', 'Facebook indefinido');
			}
			link="http://www.facebook.com/"+facebookReal;
			abrirLink(link);
		}else{
			if(facebookNeg!=undefined){
				esface="algo";
				notificaciones('Cargando', 'Por favor espere...');
				facebookReal=facebookNeg.split("facebook.com/").pop();
				location.href="https://www.facebook.com/"+facebookReal;
			}else{
				
				notificaciones('Tidingo', 'Facebook indefinido');
			}
		}
		
		
	});

	$(document).on('click','#botonGaleria',function(){
		urlPag=window.location.href;
		nombreUserPerfil=urlPag.split("/negocio.html?").pop();
		cargandoPaginas();
		location.href='fotos.html?'+nombreUserPerfil;
	});

	$(document).on('click','#fotoPrincipal',function(){
		urlPag=window.location.href;
		nombreUserPerfil=urlPag.split("/negocio.html?").pop();
		cargandoPaginas();
		location.href='fotos.html?'+nombreUserPerfil;
	});

	$(document).on('click','#llegarIr',function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			link="http://maps.google.com/maps?&z=10&q="+latitudActual+"+"+longituActual+"&ll="+latitudActual+"+"+longituActual;
			abrirLink(link);
		}else{
			location.href="http://maps.google.com/maps?&z=10&q="+latitudActual+"+"+longituActual+"&ll="+latitudActual+"+"+longituActual;
		}
	});

	$(document).on('click','#irHorarios',function(){
		if(banderaHora==0){
			crearHorariosComp();
		}else{
			eliminarHorariosUp();
		}
	});

	$(document).on('click','#cubrePantallaHorarios',function(){
		salirHorarios();
	});

	$(document).on('click','#cubrePantallaNotificaciones',function(){
		salirNotificaciones();
	});

	$(document).on('click','#cubrePantallaHorarios',function(){
		salirHorarios();
	});

});

function alturas(){
	var ancho = $( window ).width();

	$('#marginTopPorEncabezado').css('padding-top',  $('#formBusquedaLogout').outerHeight());
	$('#encabezadoNegocio').css('height', ancho * .43);
	$('#figurePerfilNegocio').css('height',  $('#figurePerfilNegocio').width());
	$('#figurePerfilNegocio').css('top',  $('#figurePerfilNegocio').css('left'));
	$('#cuadroMapa').css('height', $('#cuadroMapa').width() / 1.45);
	$('#cuadroMapa').css('margin-bottom', ancho*.04);
	$('#sobreponerMapa').css('height', $('#cuadroMapa').height());
	$('#sobreponerMapa').css('bottom', ancho*.04);
	$('.tamañosImg').css('height', ($('.tamañosImg').width()*.955));
	$('.contenedorLetras2').css('margin-top', ancho * .047);
	$('.contenedorLetras2').css('margin-bottom', ancho * .04);
	$('.contenedorLetras2Dir').css('margin-top', ancho * .047);
	$('.contenedorLetras2Dir').css('margin-bottom', ancho * .04);
	$('.contenedorLetras3').css('height', ancho * .050);
	var paddingTopLetras3=($('.contenedorLetras3').height())-($('.irTodos').height()-ancho*.003);
	$('.contenedorLetras3').css('padding-top', paddingTopLetras3 );
	var anchoRealLetras3=$('.contenedorLetras3').height()+(paddingTopLetras3+(ancho*.003));
	$('.contenedorLetras3').css('margin-top', ($('.cuadroVer').height()-(anchoRealLetras3))/2-ancho*.003);
	$('#irHorarios').css('margin-top', ancho*.01);
	$('.contenedorLetras3Dir').css('height', ancho * .05);
	$('.contenedorLetras3Dir').css('padding-top', paddingTopLetras3 + ancho*.001);
	var letras3alto=$('.contenedorLetras3Dir').height()-paddingTopLetras3*2+ancho*.001;
	$('.contenedorLetras3Dir').css('margin-top', (($('.contenedorLetras2Dir').height()+.087)/2)+(letras3alto/2));
	$('.tamañosImg').css('margin-top', ($('.cuadroVer').height()/2)-($('.tamañosImg').height()/2));
	var paddingTopGaleria=ancho*.03;
	$('#botonGaleria').css('height', ancho * .12);
	$('#botonFacebook').css('height', ancho * .12);
	$('#botonGaleria').css('margin-top', paddingTopGaleria );
	$('#botonFacebook').css('margin-top', paddingTopGaleria );
	$('#botonGaleria').css('margin-bottom', paddingTopGaleria );
	$('#cupon').css('margin-top', paddingTopGaleria );
	$('#figureCupon').css('margin-bottom', ancho*.02 );
	$('#porcentajeNum').css('top', ancho * .09);
	//$('#galeriaImg').css('padding-top', paddingTopGaleria + ancho * .009);
	cargado();
}	

$(window).resize(function(){
	alturas();
});
