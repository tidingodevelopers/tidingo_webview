$(document).on('ready',function(){
	localStorage.setItem("ultimaPag","imagenes");
	urlPag=window.location.href;
  	nombreUserPerfil=urlPag.split("/imagenes.html?").pop();
	imagenesSele = new Array();
	seleccionadabd = new Array();
	imagenesSele2 = new Array();
	seleccionadabd2 = new Array();
	
	firebase.auth().onAuthStateChanged(function(user) {
		if (!user) {

			location.href="index.html";	   
		}else{
			//console.log("entra");
            crearImagenes();
		}
	});

	$(document).on('click','#aceptar',function(){
		cargandoPaginas();
		location.href="editarNegocio.html?"+nombreUserPerfil;

	});

	$(document).on('click','#publicar',function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			choosePhoto();
		}else{
			//alert("hola");
		}

	});

	$(document).on('click','.figureFoto',function(){
		datoPicado = $(this).attr('data-title');
		datoNumero = $(this).attr('data-num');
		datoPrincipal = $(this).attr('data-principal');
		datoPicado2 = $(this).attr('data-title2');
		datoPrincipal2 = $(this).attr('data-principal2');
		if($('#divadelante'+datoNumero).css('display') == 'none'){
			$('#divadelante'+datoNumero).css('display', 'block');
			imagenesSele.push(datoPicado);
			seleccionadabd.push(datoPrincipal);
			imagenesSele2.push(datoPicado2);
			seleccionadabd2.push(datoPrincipal2);
			console.log(imagenesSele);
			console.log(seleccionadabd);
			if(imagenesSele.length<2){
				borrarImg();
				$('#publicar').remove();
			}
		}else{
			$('#divadelante'+datoNumero).css('display', 'none');
			var index = imagenesSele.indexOf(datoPicado);
			if (index > -1) {
			    imagenesSele.splice(index, 1);
			}
			var index2 = seleccionadabd.indexOf(datoPrincipal);
			if (index2 > -1) {
			    seleccionadabd.splice(index2, 1);
			}
			var index3 = imagenesSele2.indexOf(datoPicado2);
			if (index3 > -1) {
			    imagenesSele2.splice(index3, 1);
			}
			var index4 = seleccionadabd2.indexOf(datoPrincipal2);
			if (index4 > -1) {
			    seleccionadabd2.splice(index4, 1);
			}
			console.log(imagenesSele);
			console.log(seleccionadabd);
			if(imagenesSele.length<1){
				$('#borrarIm').remove();
				publicar();
			}
		}

	});

	$(document).on('click','#borrarIm',function(){
		crearDialogoSiNo("Esta seguro que desea eliminar estos elementos");
		
	});

	$(document).on('click','#textoSi',function(){

		var user = firebase.auth().currentUser;
		var storageRef = storage.ref();
		updates={};
		updates2={};
		for(var i=0; i<imagenesSele.length; i++){
			updateSelec=seleccionadabd[i];
			nameimg=imagenesSele[i];
			updateSelec2=seleccionadabd2[i];
			nameimg2=imagenesSele2[i];
			updates[updateSelec]=null;
			updates2[updateSelec2]=null;
			var desertRef = storageRef.child('users/'+user.uid+'/img500/todas/'+nameimg);
			var desertRef2 = storageRef.child('users/'+user.uid+'/img1024/todas/'+nameimg2);

			// Delete the file
			desertRef.delete().then(function() {
			  // File deleted successfully
			}).catch(function(error) {
			  // Uh-oh, an error occurred!
			});
			desertRef2.delete().then(function() {
			  // File deleted successfully
			}).catch(function(error) {
			  // Uh-oh, an error occurred!
			});
		}
		console.log(updates);
		firebase.database().ref('users/'+user.uid+'/fotosImg/img1024').update(updates2)
		firebase.database().ref('users/'+user.uid+'/fotosImg/img500').update(updates).then(function(){
			$('#borrarIm').remove();
			publicar();
			imagenesSele = new Array();
			seleccionadabd = new Array();
			imagenesSele2 = new Array();
			seleccionadabd2 = new Array();
			eliminarImagenes();
			crearImagenes();

	  	});
	  	salirDialogoSiNo();
	});

	$(document).on('click','#textoNo',function(){
		salirDialogoSiNo();

	});

	$(document).on('click','#cubrePantallaSiNo',function(){
		salirDialogoSiNo();

	});
}); // Fin de ON READY

function choosePhoto() {
	var user = firebase.auth().currentUser;
                userId = user.uid;
	uidNeg = userId;
    Ids = Android.getIds(uidNeg);
    //console.log(uidNeg)
}

function alturasImagenes(){
	var ancho = $( window ).width();

	$('#cancelar').css('padding-top',  $('#titulo').width() / 16);
	$('#cancelar').css('padding-bottom',  $('#titulo').width() / 18);
	$('#titulo').css('padding-top',  $('#titulo').width() / 16);
	$('#titulo').css('padding-bottom',  $('#titulo').width() / 18);
	$('#aceptar').css('padding-top',  $('#titulo').width() / 16);
	$('#aceptar').css('padding-bottom',  $('#titulo').width() / 18);
	$('.tresImagenes').css('height', ancho*.33);
	$('.tresImagenes').css('margin-top', ancho*.0049);
	$('.tresImagenes').css('margin-bottom', ancho*.0049);
	$('.figureFotoDelante').css('height', ancho*.33);
	$('.palomitaCuadro').css('margin-top', ancho*.13);

}

$(window).on('resize', function(){
	alturasImagenes();
});











