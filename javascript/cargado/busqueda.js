$(document).on('ready',function(){
	localStorage.setItem("ultimaPag","busqueda");
	traerDatos();

	alturas();

	$(document).on('click','.cuadroNegocio',function(){
		datoPicado = $(this).attr('data-title');
		location.href='negocio.html?'+datoPicado;
		cargandoPaginas();
	});

	$(document).on('keydown','#busquedaNegocio',function(e){
		if(e.keyCode==13){
			e.preventDefault();
			cargandoPaginas();
			enviarVar();
		}
		
	});
	$("select[name=comboDistancias]").change(function(e){
		$("#tipoBusqueda").val('distancia');
		e.preventDefault();
		cargandoPaginas();
		enviarVar();
    });
});
$(document).on('click','#normalGeo',function(e){
	$('#comboDistancias > option[value="1"]').attr('selected', 'selected');
	$("#tipoBusqueda").val('normal');
	e.preventDefault();
	cargandoPaginas();
	enviarVar();
});
/*$(document).on('click','#distanciaGeo',function(e){
	//ya no se utiliza la funcionalidad del boton
});*/
showDropdown = function (element) {
    var event;
    event = document.createEvent('MouseEvents');
    event.initMouseEvent('mousedown', true, true, window);
    element.dispatchEvent(event);
};
function alturas(){
	$('#listaNegocios').css('padding-top', $('#formBusquedaLogout').outerHeight());
}

$(window).resize(function(){
	alturas();
});