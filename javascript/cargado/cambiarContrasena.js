$(document).on('ready',function(){
	firebase.auth().onAuthStateChanged(function(user) {
		if (!user) {
			location.href="iniciarSesion.html";	   
		}
	});
	
	paginaCargada();
	alturasPassMail();

	$(document).on('click', '#cancelar', function(){
		window.history.back();
		cargandoPaginas();
	});

	$(document).on('click', '#aceptar', function(){
		cambiarContrasena();
	});

	$(document).on('click','#cubrePantallaNotificaciones',function(){
		salirNotificaciones();
	});
	
});

function alturasPassMail(){
	var ancho = $( window ).width();

	$('#cancelar').css('padding-top',  $('#titulo').width() / 16);
	$('#cancelar').css('padding-bottom',  $('#titulo').width() / 18);
	$('#titulo').css('padding-top',  $('#titulo').width() / 16);
	$('#titulo').css('padding-bottom',  $('#titulo').width() / 18);
	$('#aceptar').css('padding-top',  $('#titulo').width() / 16);
	$('#aceptar').css('padding-bottom',  $('#titulo').width() / 18);
}

$(window).resize(function(){
	alturasPassMail();
});

