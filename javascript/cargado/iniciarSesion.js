var redi=true;
$(document).on('ready', function(){
	imagenAleatori();
	
	firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
        	if(redi==true){
	            var user = firebase.auth().currentUser;
	            userId = user.uid;
	    		cargandoPaginas();
	            location.href="index.html";  
            }    
            
        }
    });

	alturasIniciarSesion();
	paginaCargada();
});

$(document).on('click','#cubrePantallaNotificaciones',function(){
	salirNotificaciones();
});

function alturasIniciarSesion(){
	var ancho = $( window ).width();

	$('#imagenesDifuminadas').css('height',  $('#imagenesDifuminadas').width() / 1.33);

	// $('#direccion').css('top', ($('#sobreponerMapa').outerHeight() -  $('#direccion').outerHeight()) / 2);
}

$(document).on('click', '#iniciarSesion', function(){
	redi=false;
	iniciarSes();
	return false;
});

$(window).resize(function(){
	alturasIniciarSesion();
});
