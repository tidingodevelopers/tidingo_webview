$(document).on('ready',function(){
	urlPag=window.location.href;
  	nombreUserPerfil=urlPag.split("/ciudades.html?").pop();
	ultimaPgina=localStorage.getItem("ultimaPag");

	paginaCargada();
	$(document).on('click ontouchstart','.ciudad',function(){
		cargandoPaginas();
		idCiudad = $(this).attr('data-municipio');
		idCiudadEdit = $(this).attr('data-municipio');
		idEstado = $(this).attr('data-estado');
		ciudadSel=idCiudad;
		estadoSel=idEstado;
		if(ultimaPgina!="editarNeg"){
			localStorage.setItem("ciudadBan", ciudadSel);
		}else{
			localStorage.setItem("ciudadEdit", ciudadSel);
			localStorage.setItem("estadoEdit", estadoSel);
		}
		if(ultimaPgina=="editarNeg"){
			localStorage.setItem("ultimaPag","ciudades");
			location.href="editarNegocio.html?"+nombreUserPerfil;
		}else if(ultimaPgina=="negocio"){

			location.href="negocio.html?"+nombreUserPerfil;
		}
		else{
			location.href="index.html";
		}
	});
});
