
$(document).on('ready',function(){
	ban1=7;
	bandera1(ban1);
	urlPag=window.location.href;
	nombreUserPerfil=urlPag.split("/editarNegocio.html?").pop();
	alturasEditarNegocio();

    firebase.auth().onAuthStateChanged(function(user) {
		if (!user) {
			location.href="iniciarSesion.html";	   
		}else{
			
            mostrarDatos();
		}
	});

	$(document).on('click', '#cancelar', function(){
		ultimapag=localStorage.getItem("ultimaPag");
		
		if(document.referrer=="http://detodoapp.com/josias/registrarNegocio.html"){
			window.location.href="index.html";
		}else if(document.referrer=="http://detodoapp.com/josias/iniciarSesion.html"){
			window.location.href="index.html";
		}else if(ultimapag=="ciudades"){
			window.location.href="negocio.html?"+nombreUserPerfil;
		}else if(ultimapag=="imagenes"){
			window.location.href="negocio.html?"+nombreUserPerfil;
		}
		else{
			window.history.back();
			cargandoPaginas();
		}
	});

	$(document).on('click', '#aceptar', function(){
		banIrCiu=0;
		subirDatos();
	});

	$(document).on('click', '#perfilCover', function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			subirPerfil();
		}else{
			//alert("hola");
		}
	});

	$(document).on('click', '#coverPrincipal', function(){
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
		if(isAndroid) {
			subirPortada();
		}else{
			//alert("hola");
		}
	});

	$(document).on('click','#botonGaleria',function(){
		banIrCiu=2;
		var nombreNegocio = $("#editarNombreNegocio").val();
		if(nombreNegocio!="" && nombreNegocio!=null){
			subirDatos();
		}

	});

	$(document).on('click','#contenedorCiudad',function(){
			banIrCiu=1;
			var nombreNegocio = $("#editarNombreNegocio").val();
			if(nombreNegocio!="" && nombreNegocio!=null){
				subirDatos();
			}
			
	});

	$(document).on('click','#seleccionarUbicacion',function(){
		//document.body.style.overflow = 'hidden';
		ban1=3;
		bandera1(ban1);

		crearMapa();
		
	});

	$(document).on('click','.checkHorario',function(){
		idHorario = $(this).attr('id');
		datacheck = $(this).attr('data-check');

		var horarioIn = document.getElementById(idHorario);
		if(datacheck=="0"){
			horarioIn.setAttribute('data-check', '1');
			var imagenAde = document.createElement('img');
			imagenAde.setAttribute('id', 'check'+idHorario);
			imagenAde.setAttribute('class', 'palomitaCuadro'); 
			imagenAde.setAttribute('src', 'objetos/negocios/checkedNegra.png');//Esta imagen sale de la D.B.
			horarioIn.appendChild(imagenAde);
		}else{
			horarioIn.setAttribute('data-check', '0');
			$('#check'+idHorario).remove();
		}
		
	});

	$(document).on('click','.inputHoraNegocio',function(){
		idSeleccionado = $(this).attr('id');

		var menuS = document.getElementById('barraCompleta');
		if(menuS!=undefined){
			eliminarPickHora();
		}

		var delayMillis = 200; //1 second

		setTimeout(function() {
		  pickerHorarios();
		}, delayMillis);
		
	});

	$(document).on('click','.escalonesHoras',function(){
		horaSelec = $(this).attr('data-title');
		if(horaSelec!="Cerrado"){
			var horaTodas = document.getElementById(idSeleccionado);
			horaTodas.value=horaSelec;
		}else{
			numeroLetras=idSeleccionado.length;
			nuevoid="hora2"+idSeleccionado.substring(5, numeroLetras);
			nuevoid2="hora1"+idSeleccionado.substring(5, numeroLetras);
			console.log(nuevoid)
			var horaTodas = document.getElementById(nuevoid2);
			var horaTodas2 = document.getElementById(nuevoid);
			horaTodas.value=horaSelec;
			horaTodas2.value=horaSelec;
		}
	});
	
	$(".inputHoraNegocio").focus(function() {
	    //console.log('in');
	}).blur(function() {
		var delayMillis = 200; //1 second

		setTimeout(function() {
		  eliminarPickHora();
		}, delayMillis);
		    
	});

});

function choosePhoto() {
	var user = firebase.auth().currentUser;
                userId = user.uid;
	uidNeg = userId;
    Ids = Android.getIds(uidNeg);
    //console.log(uidNeg)
}

function alturasEditarNegocio(){
	var ancho = $( window ).width();

	$('#cancelar').css('padding-top',  $('#titulo').width() / 16);
	$('#cancelar').css('padding-bottom',  $('#titulo').width() / 18);
	$('#titulo').css('padding-top',  $('#titulo').width() / 16);
	$('#titulo').css('padding-bottom',  $('#titulo').width() / 18);
	$('#aceptar').css('padding-top',  $('#titulo').width() / 16);
	$('#aceptar').css('padding-bottom',  $('#titulo').width() / 18);
	$('#contenedormapita').css('height', $('#contenedormapita').width() / 2.25);
	$('#sobreponerMapa').css('height', $('#contenedormapita').width() / 2.15);
	$('#sobreponerMapa').css('bottom', ancho*.19);
	$('#editarFotos').css('height',  $('#editarFotos').width() / 2.3);
	$('#editarFotoPerfil').css('width',  $('#editarFotos').width() / 4);
	$('#editarFotoPerfil').css('height',  $('#editarFotos').width() / 4);
	$('#editarFotoPerfil').css('left',  $('#editarFotos').width() / 19);
	$('#editarFotoPerfil').css('top',  ($('#editarFotos').height() - $('#editarFotoPerfil').height()) / 2.1);
	$('#perfilCover').css('width',  $('#editarFotos').width() / 4);
	$('#perfilCover').css('height',  $('#editarFotos').width() / 4);
	$('#perfilCover').css('left',  $('#editarFotos').width() / 19);
	$('#perfilCover').css('top',  ($('#editarFotos').height() - $('#editarFotoPerfil').height()) / 2.1);
}

$(window).resize(function(){
	alturasEditarNegocio();
});


// $('#fotosCompletas').css('top', $('#formBusquedaLogout').outerHeight());


// 	$('.figureFoto').css({'border-bottom-width': $('#fotosCompletas').width() / 70});