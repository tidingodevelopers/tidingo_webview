var horarioEmer;
var llamada;
var facebookNeg;
var webNeg;
var latitudActual;
var longituActual;
function miNegocio(){
	urlPag=window.location.href;
	nombreUserPerfil=urlPag.split("/negocio.html?").pop();

	starCountRef = firebase.database().ref('users/'+nombreUserPerfil);
	starCountRef.on('value', function(snapshot) {
			var padreEncabezado=document.getElementById("marginTopPorEncabezado");
	  //console.log(snapshot.val());
		datos=snapshot.val();
		//console.log(datos.nombre);
		if(datos.nombre!=""){
			if(datos.nombre!=undefined){
				var nombrePoner = document.getElementById("busquedaNegocio");
				nombrePoner.setAttribute("placeholder", datos.nombre);
			}
		}
		if(datos.descripcion!="" && datos.descripcion!=undefined){
			document.getElementById("textoDescriptivo").innerHTML = datos.descripcion;
		}
		
		if(datos.pagweb!="" && datos.pagweb!=undefined){
			webNeg=datos.pagweb;
			document.getElementById("paginaWeb").innerHTML = datos.pagweb;
		}else{
			padreEncabezado.removeChild(document.getElementById("contPagina"));
		}
		if(datos.correo!="" && datos.correo!=undefined){
			document.getElementById("correo").innerHTML = datos.correo;
		}else{
			padreEncabezado.removeChild(document.getElementById("contCorreo"));
		}
		if(datos.telefono!=undefined && datos.telefono!=""){	  
		  llamada = datos.telefono;
		  document.getElementById("telefono1").innerHTML = datos.telefono;
		}else{
			padreEncabezado.removeChild(document.getElementById("contenedorTelefono1"));
		}
		if(datos.telefono2!=undefined && datos.telefono2!=""){	  
		  llamada2 = datos.telefono2;
		  document.getElementById("telefono2").innerHTML = datos.telefono2;
		}else{
			padreEncabezado.removeChild(document.getElementById("contenedorTelefono2"));
		}
		if(datos.telefono3!=undefined && datos.telefono3!=""){	  
		  llamada3 = datos.telefono3;
		  document.getElementById("telefonoWsp").innerHTML = datos.telefono3;
		}else{
			padreEncabezado.removeChild(document.getElementById("contenedorTelefonoWsp"));
		}
		if(datos.descuento!=undefined && datos.descuento!=""){	 
		  if(datos.descuento.indexOf("%") != -1){
		  	document.getElementById("porcentajeNum").innerHTML = datos.descuento;
		  }else{
		  	document.getElementById("porcentajeNum").innerHTML = datos.descuento+"%";
		  }
		}else{
			padreEncabezado.removeChild(document.getElementById("cupon"));
		}
		if(datos.horario!=undefined && datos.horario!=""){	  
		  horarioEmer = datos.horario;
		}
		if(datos.facebook!=undefined && datos.facebook!=""){	  
		  facebookNeg = datos.facebook;
		  
		}
		if(datos.longitudN!=undefined && datos.longitudN!="" && datos.latitudN!=undefined && datos.latitudN!=""){	 
			longituActual= datos.longitudN;
			latitudActual= datos.latitudN; 
			crearMapaNegocio();
		  //var longitudNP = document.getElementById("editarLongitudNegocio");
		  //longitudNP.value=datos.longitudN;
		}else{
			$("#cuadroMapa").remove();
			$("#sobreponerMapa").remove();
			$("#llegarIr").remove();
		}
		if(datos.horarios){
			if(datos.horarios.hora1Lu!=undefined && datos.horarios.hora1Lu!="" && datos.horarios.hora1Ma!=undefined && datos.horarios.hora1Ma!="" && datos.horarios.hora1Mi!=undefined && datos.horarios.hora1Mi!="" && datos.horarios.hora1Ju!=undefined && datos.horarios.hora1Ju!="" && datos.horarios.hora1Vi!=undefined && datos.horarios.hora1Vi!="" && datos.horarios.hora1Sa!=undefined && datos.horarios.hora1Sa!="" && datos.horarios.hora1Do!=undefined && datos.horarios.hora1Do!="" && 
			datos.horarios.hora2Lu!=undefined && datos.horarios.hora2Lu!="" && datos.horarios.hora2Ma!=undefined && datos.horarios.hora2Ma!="" && datos.horarios.hora2Mi!=undefined && datos.horarios.hora2Mi!="" && datos.horarios.hora2Ju!=undefined && datos.horarios.hora2Ju!="" && datos.horarios.hora2Vi!=undefined && datos.horarios.hora2Vi!="" && datos.horarios.hora2Sa!=undefined && datos.horarios.hora2Sa!="" && datos.horarios.hora2Do!=undefined && datos.horarios.hora2Do!=""){
				
			}else{
				padreEncabezado.removeChild(document.getElementById("contHorarios"));
			}
			if(datos.horarios.hora1Lu!=undefined){	  
			  	hora1Lun = datos.horarios.hora1Lu;
			}
			if(datos.horarios.hora2Lu!=undefined){	  
				hora2Lun = datos.horarios.hora2Lu;
				 
			}
			if(datos.horarios.hora1Ma!=undefined){	  
				hora1Mar = datos.horarios.hora1Ma;
			}
			if(datos.horarios.hora2Ma!=undefined){	  
				hora2Mar = datos.horarios.hora2Ma;
			}
			if(datos.horarios.hora1Mi!=undefined){	  
				hora1Mie = datos.horarios.hora1Mi;
			}
			if(datos.horarios.hora2Mi!=undefined){	  
				hora2Mie = datos.horarios.hora2Mi;
			}
			if(datos.horarios.hora1Ju!=undefined){	  
				hora1Jue = datos.horarios.hora1Ju;
			}
			if(datos.horarios.hora2Ju!=undefined){	  
				hora2Jue = datos.horarios.hora2Ju;
			}
			if(datos.horarios.hora1Vi!=undefined){	  
				hora1Vie = datos.horarios.hora1Vi;
			}
			if(datos.horarios.hora2Vi!=undefined){	  
				hora2Vie = datos.horarios.hora2Vi;
			}
			if(datos.horarios.hora1Sa!=undefined){	  
				hora1Sab = datos.horarios.hora1Sa;
			}
			if(datos.horarios.hora2Sa!=undefined){	  
				hora2Sab = datos.horarios.hora2Sa;
			}
			if(datos.horarios.hora1Do!=undefined){	  
				hora1Dom = datos.horarios.hora1Do;
			}
			if(datos.horarios.hora2Do!=undefined){	  
				hora2Dom = datos.horarios.hora2Do;
			}
		}
		if(datos.imgPerfil!=undefined){
			if(datos.imgPerfil.img500!=undefined){
				if(datos.imgPerfil.img500.url!=undefined){	  
				  var imgPer = document.getElementById("fotoPerfil");
				  imgPer.setAttribute("src", datos.imgPerfil.img500.url);
				}
			}
		}
		if(datos.imgPortada!=undefined){
			if(datos.imgPortada.img500!=undefined){	
				if(datos.imgPortada.img500.url!=undefined){	  
				  var imgPort = document.getElementById("fotoPrincipal");
				  imgPort.setAttribute("src", datos.imgPortada.img500.url);
				}
			}
		}

		document.getElementById("direccion").innerHTML = datos.calle+" "+"<br>"+datos.municipio + " "+ datos.estado;
		alturas();
		paginaCargada();
	});
}


function llamarTel(lamNu) {
	if(lamNu==1){
    	getllam = Android.llamarTel(llamada);
	}
	if(lamNu==2){
    	getllam = Android.llamarTel(llamada2);
	}
	if(lamNu==3){
    	getllam = Android.llamarTel(llamada3);
	}
    //console.log(uidNeg)
}

function abrirLink(link) {

	getLink = Android.abrirLink(link);
    //console.log(uidNeg)
}