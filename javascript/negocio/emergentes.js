function horarios(){
	var menu = document.getElementById('menu');

	var cubrePantalla = document.createElement('div');
	cubrePantalla.setAttribute('id', 'cubrePantallaHorarios');

	var cuadroCentrado = document.createElement('div');
	cuadroCentrado.setAttribute('id', 'cuadroCentradoHorarios');
	cuadroCentrado.setAttribute('class', 'quincePixeles');

	var titulo = document.createElement('p');
	titulo.setAttribute('id', 'tituloHorarios');
	titulo.setAttribute('class', 'cuarentaYCincoPixeles');
	var textoTitulo = document.createTextNode('Horario comercial');
	titulo.appendChild(textoTitulo);
	cuadroCentrado.appendChild(titulo);
	$(titulo).css('padding-top', $(document).width() / 23);
	$(titulo).css('padding-bottom', $(document).width() / 23);

	var horario = document.createElement('p');
	horario.setAttribute('id', 'horarioNegocio');
	horario.setAttribute('class', 'cuarentaYCincoPixeles');
	if(horarioEmer!=undefined){
		var textoHorarios = document.createTextNode(horarioEmer);//Info de D.B
	}else{
		var textoHorarios = document.createTextNode("horario no actualizado");
	}
	horario.appendChild(textoHorarios);
	cuadroCentrado.appendChild(horario);
	$(horario).css('padding-top', $(document).width() / 12);
	$(horario).css('padding-bottom', $(document).width() / 12);


	document.body.insertBefore(cubrePantalla, menu);
	document.body.insertBefore(cuadroCentrado, menu);

	cargado();		

	$(cuadroCentrado).css('top', ($(document).height() - $(cuadroCentrado).height()) / 2);

}

$(window).resize(function(){
	$('#cuadroCentradoHorarios').css('top', ($(document).height() - $('#cuadroCentradoHorarios').height()) / 2);
	$('#tituloHorarios').css('padding-top', $(document).width() / 23);
	$('#tituloHorarios').css('padding-bottom', $(document).width() / 23);
	$('#horarioNegocio').css('padding-top', $(document).width() / 15);
	$('#horarioNegocio').css('padding-bottom', $(document).width() / 15);
});

function salirHorarios(){
	document.body.removeChild(cubrePantallaHorarios);
	document.body.removeChild(cuadroCentradoHorarios);
}

