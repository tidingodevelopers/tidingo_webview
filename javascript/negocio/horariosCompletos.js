contHorariosTam=0;
var hora2Lun;
var hora1Lun;
var hora2Dom;
var hora1Dom;
var hora2Mar;
var hora1Mar;
var hora2Mie;
var hora1Mie;
var hora2Jue;
var hora1Jue;
var hora2Vie;
var hora1Vie;
var hora2Sab;
var hora1Sab;
var num1=0;
var num2=0;
var num3=0;
var num4=0;
var num5=0;
var num6=0;
var num7=0;
function crearHorariosComp(){
	contHorariosTam = $( "#contHorarios" ).height();

	var menuS = document.getElementById('barraCompleta');
	var barraArribaB = document.getElementById("contHorarios");
	var topEncabezado = document.getElementById("marginTopPorEncabezado");

	var barraCompleta = document.createElement('div');
	barraCompleta.setAttribute('id', 'barraCompleta');
	barraCompleta.setAttribute('class','quincePixeles');
	barraArribaB.appendChild( barraCompleta);

	var primerEscalon = document.createElement('div');
		primerEscalon.setAttribute('class', 'escalonesDias quincePixeles');
		primerEscalon.setAttribute('id', 'primerEscalon');
		barraCompleta.appendChild(primerEscalon);

	var primerEscalonH = document.createElement('div');
		primerEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
		primerEscalonH.setAttribute('id', 'primerEscalonH');
		barraCompleta.appendChild(primerEscalonH);

	var segundoEscalon = document.createElement('div');
	segundoEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	segundoEscalon.setAttribute('id', 'segundoEscalon');
	barraCompleta.appendChild(segundoEscalon);

	var segundoEscalonH = document.createElement('div');
	segundoEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	segundoEscalonH.setAttribute('id', 'segundoEscalonH');
	barraCompleta.appendChild(segundoEscalonH);

	var tercerEscalon = document.createElement('div');
	tercerEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	tercerEscalon.setAttribute('id', 'tercerEscalon');
	barraCompleta.appendChild(tercerEscalon);

	var tercerEscalonH = document.createElement('div');
	tercerEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	tercerEscalonH.setAttribute('id', 'tercerEscalonH');
	barraCompleta.appendChild(tercerEscalonH);

	var cuartoEscalon = document.createElement('div');
	cuartoEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	cuartoEscalon.setAttribute('id', 'cuartoEscalon');
	barraCompleta.appendChild(cuartoEscalon);

	var cuartoEscalonH = document.createElement('div');
	cuartoEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	cuartoEscalonH.setAttribute('id', 'cuartoEscalonH');
	barraCompleta.appendChild(cuartoEscalonH);

	var quintoEscalon = document.createElement('div');
	quintoEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	quintoEscalon.setAttribute('id', 'quintoEscalon');
	barraCompleta.appendChild(quintoEscalon);

	var quintoEscalonH = document.createElement('div');
	quintoEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	quintoEscalonH.setAttribute('id', 'quintoEscalonH');
	barraCompleta.appendChild(quintoEscalonH);

	var sextoEscalon = document.createElement('div');
	sextoEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	sextoEscalon.setAttribute('id', 'sextoEscalon');
	barraCompleta.appendChild(sextoEscalon);

	var sextoEscalonH = document.createElement('div');
	sextoEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	sextoEscalonH.setAttribute('id', 'sextoEscalonH');
	barraCompleta.appendChild(sextoEscalonH);

	var septimoEscalon = document.createElement('div');
	septimoEscalon.setAttribute('class', 'escalonesDias quincePixeles');
	septimoEscalon.setAttribute('id', 'septimoEscalon');
	barraCompleta.appendChild(septimoEscalon);

	var septimoEscalonH = document.createElement('div');
	septimoEscalonH.setAttribute('class', 'escalonesHora quincePixeles');
	septimoEscalonH.setAttribute('id', 'septimoEscalonH');
	barraCompleta.appendChild(septimoEscalonH);

	if(hora1Dom!=undefined && hora1Dom!=""){
		num1=1;
		var domingo= document.createElement('p');
		var domingoText =  document.createTextNode('Domingo');
		domingo.setAttribute('class', 'diasTexto cuarentaPixeles');
		domingo.appendChild(domingoText);
		primerEscalon.appendChild(domingo); 

		var domingoHora = document.createElement('p');
		var domingoHoraText =  document.createTextNode(hora1Dom+" - "+hora2Dom);
		domingoHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		domingoHora.appendChild(domingoHoraText);
		primerEscalonH.appendChild(domingoHora);
	} 

	if(hora1Lun!=undefined && hora1Lun!=""){
		num2=1;
		var lunes = document.createElement('p');
		var lunesText =  document.createTextNode('Lunes');
		lunes.setAttribute('class', 'diasTexto cuarentaPixeles');
		lunes.appendChild(lunesText);
		segundoEscalon.appendChild(lunes); 

		var lunesHora = document.createElement('p');
		var lunesTextHora =  document.createTextNode(hora1Lun+" - "+hora2Lun);
		lunesHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		lunesHora.appendChild(lunesTextHora);
		segundoEscalonH.appendChild(lunesHora); 
	}

	if(hora1Mar!=undefined && hora1Mar!=""){
		num3=1;
		var martes = document.createElement('p');
		var martesText =  document.createTextNode('Martes');
		martes.setAttribute('class', 'diasTexto cuarentaPixeles');
		martes.appendChild(martesText);
		tercerEscalon.appendChild(martes);

		var martesHora = document.createElement('p');
		var martesTextHora =  document.createTextNode(hora1Mar+" - "+hora2Mar);
		martesHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		martesHora.appendChild(martesTextHora);
		tercerEscalonH.appendChild(martesHora); 
	}

	if(hora1Mie!=undefined && hora1Mie!=""){
		num4=1;
		var miercoles = document.createElement('p');
		var miercolesText =  document.createTextNode('Miercoles');
		miercoles.setAttribute('class', 'diasTexto cuarentaPixeles');
		miercoles.appendChild(miercolesText);
		cuartoEscalon.appendChild(miercoles);

		var miercolesHora = document.createElement('p');
		var miercolesTextHora =  document.createTextNode(hora1Mie+" - "+hora2Mie);
		miercolesHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		miercolesHora.appendChild(miercolesTextHora);
		cuartoEscalonH.appendChild(miercolesHora); 
	}

	if(hora1Jue!=undefined && hora1Jue!=""){
		num5=1;
		var jueves = document.createElement('p');
		var juevesText =  document.createTextNode('Jueves');
		jueves.setAttribute('class', 'diasTexto cuarentaPixeles');
		jueves.appendChild(juevesText);
		quintoEscalon.appendChild(jueves);

		var juevesHora = document.createElement('p');
		var juevesTextHora =  document.createTextNode(hora1Jue+" - "+hora2Jue);
		juevesHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		juevesHora.appendChild(juevesTextHora);
		quintoEscalonH.appendChild(juevesHora); 
	}

	if(hora1Vie!=undefined && hora1Vie!=""){
		num6=1;
		var viernes = document.createElement('p');
		var viernesText =  document.createTextNode('Viernes');
		viernes.setAttribute('class', 'diasTexto cuarentaPixeles');
		viernes.appendChild(viernesText);
		sextoEscalon.appendChild(viernes);

		var viernesHora = document.createElement('p');
		var viernesTextHora =  document.createTextNode(hora1Vie+" - "+hora2Vie);
		viernesHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		viernesHora.appendChild(viernesTextHora);
		sextoEscalonH.appendChild(viernesHora); 
	}

	if(hora1Sab!=undefined && hora1Sab!=""){
		num7=1;
		var sabado = document.createElement('p');
		var sabadoText =  document.createTextNode('Sabado');
		sabado.setAttribute('class', 'diasTexto cuarentaPixeles');
		sabado.appendChild(sabadoText);
		septimoEscalon.appendChild(sabado);

		var sabadoHora = document.createElement('p');
		var sabadoTextHora =  document.createTextNode(hora1Sab+" - "+hora2Sab);
		sabadoHora.setAttribute('class', 'horaTexto cuarentaPixeles');
		sabadoHora.appendChild(sabadoTextHora);
		septimoEscalonH.appendChild(sabadoHora); 
	}
	escalonTam=$("#barraCompleta").height();
	escalonTam2=$(".horaTexto").height();
	numtotal=(num1+num2+num3+num4+num5+num6+num7);
	escComp=(contHorariosTam*1.02)*numtotal;
	numMul=escComp+contHorariosTam;
	console.log(numMul);
	/*$( ".textoEscalones" ).animate({
	    opacity: 1,
	}, 2000, function() {
	    // Animation complete.
	});*/
	
	$( "#contHorarios" ).animate({
	    height: numMul,
	}, 400, function() {
		
	});

	banderaHora=1;
	cargado();
}

$(window).resize(function(){
	
});

function eliminarHorariosUp(){
	var barraArribaB = document.getElementById("contHorarios");
	barraLat=$( "#contHorarios" );
	if(barraLat!=undefined) {
		barraArribaB.removeChild(barraCompleta);
		$( "#contHorarios" ).animate({
		    height: contHorariosTam,
		}, 400, function() {
			if(banderaHora==1){
		    	banderaHora=0;
			}
		});
	}
}