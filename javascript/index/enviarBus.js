function enviarVar(){
	
	variableBusq=String($("#busquedaNegocio").val());
	variableBusq = quitaAcentos2(variableBusq);
	pasarVariables("busqueda.html", "variableBusq");
	
}

function pasarVariables(pagina, nombres) {
	tipoBusqueda=String($("#tipoBusqueda").val());
	comboDistancias=String($("#comboDistancias").val());

	if(tipoBusqueda=="" || tipoBusqueda=='' || tipoBusqueda=='undefined'){
		tipoBusqueda = "normal";
	}
	if(comboDistancias=="" || comboDistancias=='' || comboDistancias=='undefined'){
		comboDistancias = "1";
	}
	  pagina +="?tipoBusqueda="+tipoBusqueda+"&distancia="+comboDistancias+"&";
	  nomVec = nombres.split(",");
	  for (i=0; i<nomVec.length; i++)
	    pagina += nomVec[i] + "=" + escape(eval(nomVec[i]))+"&";
	  pagina = pagina.substring(0,pagina.length-1);
	  location.href=pagina;
}

function quitaAcentos2(str){ 
      for (var i=0;i<str.length;i++){ 
      //Sustituye "á é í ó ú" 
          if (str.charAt(i)=="á") str = str.replace(/á/,"a"); 
          if (str.charAt(i)=="é") str = str.replace(/é/,"e"); 
          if (str.charAt(i)=="í") str = str.replace(/í/,"i"); 
          if (str.charAt(i)=="ó") str = str.replace(/ó/,"o"); 
          if (str.charAt(i)=="ú") str = str.replace(/ú/,"u");
          if (str.charAt(i)=="ñ") str = str.replace(/ñ/,"n");
          
       } 
      return str; 
}
