var esface;
var divDegradado
function notificaciones(tituloide , textillo){
	if(esface!=null){
		ban1=0;
	}else{
		ban1=2;	
	}
	
	bandera1(ban1);
	var menu = document.getElementById('menu');

	var cancelar = document.getElementById('cancelar');

	var cubrePantalla = document.createElement('div');
	cubrePantalla.setAttribute('id', 'cubrePantallaNotificaciones');

	var cuadroCentrado = document.createElement('div');
	cuadroCentrado.setAttribute('id', 'cuadroCentradoNotificaciones');
	cuadroCentrado.setAttribute('class', 'quincePixeles');

	var titulo = document.createElement('p');
	titulo.setAttribute('id', 'tituloNotificaciones');
	titulo.setAttribute('class', 'cuarentaYCincoPixeles');
	var textoTitulo = document.createTextNode(tituloide);
	titulo.appendChild(textoTitulo);
	cuadroCentrado.appendChild(titulo);
	$(titulo).css('padding-top', $(document).width() / 23);
	$(titulo).css('padding-bottom', $(document).width() / 23);

	var parrafo = document.createElement('p');
	parrafo.setAttribute('id', 'textoNotificaciones');
	parrafo.setAttribute('class', 'cuarentaYCincoPixeles');
	var textoNotificaciones = document.createTextNode(textillo);
	parrafo.appendChild(textoNotificaciones);
	cuadroCentrado.appendChild(parrafo);
	$(parrafo).css('padding-top', $(document).width() / 12);
	$(parrafo).css('padding-bottom', $(document).width() / 12);



	if(menu != null){
		document.body.insertBefore(cubrePantalla, menu);
		document.body.insertBefore(cuadroCentrado, menu);
	}else{
		document.body.insertBefore(cubrePantalla, cancelar);
		document.body.insertBefore(cuadroCentrado, cancelar);
	}

	cargado();		

	$(cuadroCentrado).css('top', ($(window).height() - $(cuadroCentrado).height()) / 2);

}

$(window).resize(function(){
	$('#cuadroCentradoNotificaciones').css('top', ($(window).height() - $('#cuadroCentradoNotificaciones').height()) / 2);
	$('#tituloNotificaciones').css('padding-top', $(document).width() / 23);
	$('#tituloNotificaciones').css('padding-bottom', $(document).width() / 23);
	$('#textoNotificaciones').css('padding-top', $(document).width() / 15);
	$('#textoNotificaciones').css('padding-bottom', $(document).width() / 15);
});

function salirNotificaciones(){
	ban1=0;
	bandera1(ban1);
	document.body.removeChild(cubrePantallaNotificaciones);
	document.body.removeChild(cuadroCentradoNotificaciones);
}

function imagenAleatori(){
	console.log("entra");
	var imagenPonPrincipal = new Array();
	var imagenPrincipal = ["objetos/principal2.jpg","objetos/principal3.jpg","objetos/principal4.jpg","objetos/principal5.jpg","objetos/principal6.jpg"];
	i = imagenPrincipal.length;
    j = 0;

	while (i--) {
	    j = Math.floor(Math.random() * (i+1));
	    imagenPonPrincipal.push(imagenPrincipal[j]);
	    imagenPrincipal.splice(j,1);
	}

	anchoPrinc=$(window).width();
	alturaPrinc = anchoPrinc*.77;
	alturaDivDe = anchoPrinc*.17;

	var contenedorDif=document.getElementById("imagenesDifuminadas");
	divDegradado=document.createElement('div');
	divDegradado.setAttribute("id", "divDegradado");
	divDegradado.setAttribute("class", "quincePixeles");
	contenedorDif.appendChild(divDegradado);
	
	margin=alturaPrinc-alturaDivDe;
	$(divDegradado).css('top', margin);
	$(divDegradado).css('height', alturaDivDe);
	$(divDegradado).css('z-index', "2"); 

	imgPrinc=document.getElementById("imgPrincRota");
	imgPrinc.setAttribute("src", imagenPonPrincipal[0]);

	$(imgPrinc).css('height', alturaPrinc); 

	$( "#imgPrincRota" ).animate({
	    opacity: 1,
	}, 6000, function() {
	    // Animation complete.
	});

}

$(window).resize(function(){
	imgPrinc=document.getElementById("imgPrincRota");

	anchoPrinc=$(imgPrinc).width();
	alturaPrinc = anchoPrinc*.77;
	$(imgPrinc).css('height', alturaPrinc); 

	alturaDivDe = anchoPrinc*.17;
	margin=alturaPrinc-alturaDivDe;
	if(divDegradado!=undefined){
		$(divDegradado).css('top', margin);
		$(divDegradado).css('height', alturaDivDe);
		$(divDegradado).css('z-index', "2"); 
	}
});