function cambiarCursorPerfil(){
	$('#encabezadoFotoPerfil').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoFotoPerfil').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#encabezadoOpciones').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoOpciones').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#encabezadoFigureQueContieneLogo').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoFigureQueContieneLogo').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#cambiarPortada').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#cambiarPortada').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#actualizarFotoPortada').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#actualizarFotoPortada').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#actualizarFotoPerfil').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#actualizarFotoPerfil').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#actualizarFotoPerfil').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#actualizarFotoPerfil').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#cerrarSesion',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#cerrarSesion',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#crearNegocioTexto',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#crearNegocioTexto',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.miNegocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.miNegocio',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#promocionarCampana',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#promocionarCampana',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#inicio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#inicio',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#mensajes',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#mensajes',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#notificaciones',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#notificaciones',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#publicar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#publicar',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#divEditar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#divEditar',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#timeline',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#timeline',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#lugares',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#lugares',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#fotos',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#fotos',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#followers',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#followers',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#following',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#following',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#informacionBarra',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#informacionBarra',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.botonNotFollowing',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.botonNotFollowing',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.botonFollowing',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.botonFollowing',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.nombreDeFollower',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.nombreDeFollower',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.figureFotosUsuario',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.figureFotosUsuario',function(){
		document.body.style.cursor = 'auto';
	});
}

function cambiarCursorIndex(){
	$(document).on('mouseenter','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','#encabezadoBotonRegistrar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoBotonRegistrar',function(){
		document.body.style.cursor = 'auto';
	});		

	$(document).on('mouseenter','#encabezadoBotonIngresar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoBotonIngresar',function(){
		document.body.style.cursor = 'auto';
	});		

	$(document).on('mouseenter','.negocioPrincipal',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocioPrincipal',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','.negocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocio',function(){
		document.body.style.cursor = 'auto';
	});	
}

function cambiarCursorIndexLogin(){
	$(document).on('mouseenter','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'auto';
	});	

	$('#encabezadoOpciones').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoOpciones').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#encabezadoFotoPerfil').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoFotoPerfil').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.negocioPrincipal',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocioPrincipal',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','.negocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocio',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','#cerrarSesion',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#cerrarSesion',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#crearNegocioTexto',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#crearNegocioTexto',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.miNegocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.miNegocio',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#promocionarCampana',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#promocionarCampana',function(){
		document.body.style.cursor = 'auto';
	});
}

function cambiarCursorBusqueda(){
	$(document).on('mouseenter','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','#encabezadoBotonRegistrar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoBotonRegistrar',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','#encabezadoBotonIngresar',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoBotonIngresar',function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','.negocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocio',function(){
		document.body.style.cursor = 'auto';
	});	
}

function cambiarCursorBusquedaLogin(){
	$(document).on('mouseenter','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#encabezadoFigureQueContieneLogo',function(){
		document.body.style.cursor = 'auto';
	});	

	
	$('#encabezadoOpciones').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoOpciones').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});

	$('#encabezadoFotoPerfil').mouseenter(function(){
		document.body.style.cursor = 'pointer';
	});

	$('#encabezadoFotoPerfil').mouseleave(function(){
		document.body.style.cursor = 'auto';
	});	

	$(document).on('mouseenter','#cerrarSesion',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#cerrarSesion',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#crearNegocioTexto',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#crearNegocioTexto',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.miNegocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.miNegocio',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','#promocionarCampana',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','#promocionarCampana',function(){
		document.body.style.cursor = 'auto';
	});

	$(document).on('mouseenter','.negocio',function(){
		document.body.style.cursor = 'pointer';
	});

	$(document).on('mouseleave','.negocio',function(){
		document.body.style.cursor = 'auto';
	});	
}

