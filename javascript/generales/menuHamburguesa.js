var anchoTotal;
var anchoLateral;
estacreadoMenu=0;
function menuHamburguesa(){
	estacreadoMenu=1;
		ciudadBurger=localStorage.getItem("ciudadBan");

		var menu = document.getElementById('menu');

		var cubrePantalla = document.createElement('div');
		cubrePantalla.setAttribute('id', 'cubrePantalla');

		var barraLateralPadre = document.createElement('div');
		barraLateralPadre.setAttribute('id', 'menuBarraLateralPadre');
		barraLateralPadre.setAttribute('draggable', 'true');

		var barraLateral = document.createElement('div');
		barraLateral.setAttribute('id', 'menuBarraLateral');
		barraLateral.setAttribute('class', 'quincePixeles');
		barraLateralPadre.appendChild(barraLateral);

		var divEncabezado = document.createElement('div');
		divEncabezado.setAttribute('id', 'encabezadoMenu');
		divEncabezado.setAttribute('class', 'quincePixeles');
		barraLateral.appendChild(divEncabezado);

		var primerEscalonBuscar = document.createElement('div');
		primerEscalonBuscar.setAttribute('class', 'escalonesMenu quincePixeles');
		primerEscalonBuscar.setAttribute('id', 'primerEscalonBuscar');
		barraLateral.appendChild(primerEscalonBuscar);

		var primerEscalonCiudad = document.createElement('div');
		primerEscalonCiudad.setAttribute('class', 'escalonesMenu quincePixeles');
		primerEscalonCiudad.setAttribute('id', 'primerEscalonCiudades');
		barraLateral.appendChild(primerEscalonCiudad);

		var primerEscalon = document.createElement('div');
		primerEscalon.setAttribute('class', 'escalonesMenu quincePixeles');
		primerEscalon.setAttribute('id', 'primerEscalon');
		barraLateral.appendChild(primerEscalon);

		var segundoEscalon = document.createElement('div');
		segundoEscalon.setAttribute('class', 'escalonesMenu quincePixeles');
		segundoEscalon.setAttribute('id', 'segundoEscalon');
		barraLateral.appendChild(segundoEscalon);

		var tercerEscalon = document.createElement('div');
		tercerEscalon.setAttribute('class', 'escalonesMenu quincePixeles');
		tercerEscalon.setAttribute('id', 'tercerEscalon');
		barraLateral.appendChild(tercerEscalon);

		var cuartoEscalon = document.createElement('div');
		cuartoEscalon.setAttribute('class', 'escalonesMenu quincePixeles');
		cuartoEscalon.setAttribute('id', 'cuartoEscalon');
		barraLateral.appendChild(cuartoEscalon);

		document.body.insertBefore(cubrePantalla, menu);
		document.body.insertBefore(barraLateralPadre, menu);

	if(isItLogin()){ // CÓDIGO EJECUTABLE SOLO CON LOGIN

		var figureLogo = document.createElement('figure'); 
		figureLogo.setAttribute('id', 'logoPerfil');
		figureLogo.setAttribute('class', 'quincePixeles');
		divEncabezado.appendChild(figureLogo);

		var logo = document.createElement('img');
		logo.setAttribute('id', 'logoPer');
		logo.setAttribute('src', 'objetos/negocios/perfil.png');//JOSI, SACA ESTE LOGO DE LA D.B.
		figureLogo.appendChild(logo);

		var parrafoNombreNegocio = document.createElement('p');
		parrafoNombreNegocio.setAttribute('id', 'nomBurger');
		//JOSI SACA ESTE NOMBRE DE LA BASE DE DATOS
		parrafoNombreNegocio.setAttribute('class', 'nombreNegocioMenuHamburguesa cuarentaPixeles');
	
		divEncabezado.appendChild(parrafoNombreNegocio); 

		var figureBuscar = document.createElement('figure');
		figureBuscar.setAttribute('class', 'figureMenu quincePixeles');

		var imagenBuscar = document.createElement('img');
		imagenBuscar.setAttribute('src', 'objetos/menu/search.png');
		figureBuscar.appendChild(imagenBuscar);
		primerEscalonBuscar.appendChild(figureBuscar);

		var parrafoBuscar = document.createElement('p');
		var textoBuscar =  document.createTextNode('Buscar');
		parrafoBuscar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoBuscar.appendChild(textoBuscar);
		primerEscalonBuscar.appendChild(parrafoBuscar); 

		var figureCiudad = document.createElement('figure');
		figureCiudad.setAttribute('id', 'ciudadImagenMenu');
		figureCiudad.setAttribute('class', 'figureMenu quincePixeles');

		var imagenCiudad = document.createElement('img');
		imagenCiudad.setAttribute('src', 'objetos/menu/cities.png');
		figureCiudad.appendChild(imagenCiudad);
		primerEscalonCiudad.appendChild(figureCiudad);

		var parrafoCiudad = document.createElement('p');
		var textoCiudad =  document.createTextNode('Ciudades  ('+ciudadBurger+')');
		parrafoCiudad.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoCiudad.appendChild(textoCiudad);
		primerEscalonCiudad.appendChild(parrafoCiudad); 

		var figureEditar = document.createElement('figure');
		figureEditar.setAttribute('class', 'figureMenu quincePixeles');

		var imagenEditar = document.createElement('img');
		imagenEditar.setAttribute('src', 'objetos/menu/editar.png');
		figureEditar.appendChild(imagenEditar);
		primerEscalon.appendChild(figureEditar);

		var parrafoEditar = document.createElement('p');
		var textoEditar =  document.createTextNode('Editar negocio');
		parrafoEditar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoEditar.appendChild(textoEditar);
		primerEscalon.appendChild(parrafoEditar); 

		var figureCorreo = document.createElement('figure');
		figureCorreo.setAttribute('class', 'figureMenu quincePixeles');

		var imagenCorreo = document.createElement('img');
		imagenCorreo.setAttribute('src', 'objetos/menu/correo.png');
		figureCorreo.appendChild(imagenCorreo);
		segundoEscalon.appendChild(figureCorreo);

		var parrafoCorreo = document.createElement('p');
		var textoCorreo =  document.createTextNode('Cambiar correo');
		parrafoCorreo.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoCorreo.appendChild(textoCorreo);
		segundoEscalon.appendChild(parrafoCorreo); 

		var figureContrasena = document.createElement('figure');
		figureContrasena.setAttribute('class', 'figureMenu quincePixeles');

		var imagenContrasena = document.createElement('img');
		imagenContrasena.setAttribute('src', 'objetos/menu/contrasena.png');
		figureContrasena.appendChild(imagenContrasena);
		tercerEscalon.appendChild(figureContrasena);

		var parrafoContrasena = document.createElement('p');
		var textoContrasena =  document.createTextNode('Cambiar contraseña');
		parrafoContrasena.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoContrasena.appendChild(textoContrasena);
		tercerEscalon.appendChild(parrafoContrasena); 

		var figureCerrar = document.createElement('figure');
		figureCerrar.setAttribute('class', 'figureMenu quincePixeles');

		var imagenCerrar = document.createElement('img');
		imagenCerrar.setAttribute('src', 'objetos/menu/cerrarSesion.png');
		figureCerrar.appendChild(imagenCerrar);
		cuartoEscalon.appendChild(figureCerrar);

		var parrafoCerrar = document.createElement('p');
		var textoCerrar =  document.createTextNode('Cerrar sesión');
		parrafoCerrar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoCerrar.appendChild(textoCerrar);
		cuartoEscalon.appendChild(parrafoCerrar); 

	}else{ // CÓDIGO EJECUTABLE SOLO CON LOGOUT
		var figureLogo = document.createElement('figure'); 
		figureLogo.setAttribute('id', 'menuFigureLogo');
		figureLogo.setAttribute('class', 'quincePixeles');
		divEncabezado.appendChild(figureLogo);

		var logo = document.createElement('img'); 
		logo.setAttribute('src', 'objetos/menu/logo.png');
		figureLogo.appendChild(logo);

		var figureBuscar = document.createElement('figure');
		figureBuscar.setAttribute('class', 'figureMenu quincePixeles');

		var imagenBuscar = document.createElement('img');
		imagenBuscar.setAttribute('src', 'objetos/menu/search.png');
		figureBuscar.appendChild(imagenBuscar);
		primerEscalonBuscar.appendChild(figureBuscar);

		var parrafoBuscar = document.createElement('p');
		var textoBuscar =  document.createTextNode('Buscar');
		parrafoBuscar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoBuscar.appendChild(textoBuscar);
		primerEscalonBuscar.appendChild(parrafoBuscar); 

		var figureCiudad = document.createElement('figure');
		figureCiudad.setAttribute('id', 'ciudadImagenMenu');
		figureCiudad.setAttribute('class', 'figureMenu quincePixeles');

		var imagenCiudad = document.createElement('img');
		imagenCiudad.setAttribute('src', 'objetos/menu/cities.png');
		figureCiudad.appendChild(imagenCiudad);
		primerEscalonCiudad.appendChild(figureCiudad);

		var parrafoCiudad = document.createElement('p');
		var textoCiudad =  document.createTextNode('Ciudades  ('+ciudadBurger+')');
		parrafoCiudad.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoCiudad.appendChild(textoCiudad);
		primerEscalonCiudad.appendChild(parrafoCiudad); 

		var figureFacebook = document.createElement('figure');
		figureFacebook.setAttribute('class', 'figureMenu quincePixeles');

		var imagenFacebook = document.createElement('img');
		imagenFacebook.setAttribute('src', 'objetos/menu/facebook.png');
		figureFacebook.appendChild(imagenFacebook);
		primerEscalon.appendChild(figureFacebook);

		var parrafoFacebook = document.createElement('p');
		var textoFacebook =  document.createTextNode('Síguenos en Facebook');
		parrafoFacebook.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoFacebook.appendChild(textoFacebook);
		primerEscalon.appendChild(parrafoFacebook); 

		var figureCorreo = document.createElement('figure');
		figureCorreo.setAttribute('class', 'figureMenu quincePixeles');

		var imagenCorreo = document.createElement('img');
		imagenCorreo.setAttribute('src', 'objetos/menu/correo.png');
		figureCorreo.appendChild(imagenCorreo);
		segundoEscalon.appendChild(figureCorreo);

		var parrafoCorreo = document.createElement('p');
		var textoCorreo =  document.createTextNode('info@tidingo.com');
		parrafoCorreo.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoCorreo.appendChild(textoCorreo);
		segundoEscalon.appendChild(parrafoCorreo); 

		var figureRegistro = document.createElement('figure');
		figureRegistro.setAttribute('class', 'figureMenu quincePixeles');

		var imagenRegistro = document.createElement('img');
		imagenRegistro.setAttribute('src', 'objetos/menu/registrar.png');
		figureRegistro.appendChild(imagenRegistro);
		tercerEscalon.appendChild(figureRegistro);

		var parrafoRegistrar = document.createElement('p');
		var textoRegistrar =  document.createTextNode('Registrar negocio');
		parrafoRegistrar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoRegistrar.appendChild(textoRegistrar);
		tercerEscalon.appendChild(parrafoRegistrar); 

		var figureIngresar = document.createElement('figure');
		figureIngresar.setAttribute('class', 'figureMenu quincePixeles');

		var imagenIngresar = document.createElement('img');
		imagenIngresar.setAttribute('src', 'objetos/menu/iniciar.png');
		figureIngresar.appendChild(imagenIngresar);
		cuartoEscalon.appendChild(figureIngresar);

		var parrafoIngresar = document.createElement('p');
		var textoIngresar =  document.createTextNode('Iniciar sesión');
		parrafoIngresar.setAttribute('class', 'textoEscalones cuarentaPixeles');
		parrafoIngresar.appendChild(textoIngresar);
		cuartoEscalon.appendChild(parrafoIngresar); 
	}//Fin del else 

	cargado();		

	$('#logoPerfil').css('height', $('#logoPerfil').width());



}

$(window).on('resize', function(){
	$('#logoPerfil').css('height', $('#logoPerfil').width());
});

function salirMenu(){
	document.body.removeChild(cubrePantalla);
	document.body.removeChild(menuBarraLateralPadre);
}

function fotoBurger(){
	var user = firebase.auth().currentUser;
	if(user!=undefined){
		starCountRef = firebase.database().ref('users/'+user.uid);
		starCountRef.on('value', function(snapshot) {
		  //console.log(snapshot.val());
			datos=snapshot.val();

			if(datos.nombre!=undefined){	  
				var nomBurger = document.getElementById("nomBurger");
				var textoNombreBurger =  document.createTextNode(datos.nombre);
				nomBurger.appendChild(textoNombreBurger);
			}
			if(datos.imgPerfil!=undefined){
				if(datos.imgPerfil.img500.url!=undefined){	  
					var imgBur = document.getElementById("logoPer");
					imgBur.setAttribute("src", datos.imgPerfil.img500.url);
				}
			}
		});
	}
}

function arrastrarMenu(){
	var menu = document.getElementById('menu');

	var dragLateral = document.createElement('div');
	dragLateral.setAttribute('id', 'lateralDrag');
	dragLateral.setAttribute('draggable', 'true');
	dragLateral.setAttribute('ondragover', 'allowDrop(event)');
	dragLateral.setAttribute('ondragstart', 'drag(event)');
	dragLateral.setAttribute('ondrop', 'drop(event)');
	dragLateral.setAttribute('ondragleave', 'leaveDrag(event)');

	var dragLateralCompleto = document.createElement('div');
	dragLateralCompleto.setAttribute('id', 'dragLateralCompleto');
	dragLateralCompleto.setAttribute('ondragover', 'allowDrop(event)');
	dragLateralCompleto.setAttribute('ondrop', 'drop(event)');
	dragLateralCompleto.setAttribute('ondragleave', 'leaveDrag(event)');
	

	document.body.insertBefore(dragLateral, menu);
	document.body.insertBefore(dragLateralCompleto, menu);

	anchoPrinc=$(window).width();
	anchoTotal=anchoPrinc*.70;
}

function drag(ev) {
	
    //console.log("entra"+anchoTotal);
}

/*
function allowDrop(ev) {
	ev.preventDefault();
	console.log("entrandodrop22");
	anchoLateral = $("#lateralDrag").width();
    $("#lateralDrag").css('width', ev.clientX);
    $("#lateralDrag").css('background', 'blue');
    
}


function drop(ev) {
	console.log("entra"+anchoTotal);
   	if((anchoTotal/2)>=anchoLateral){
   		console.log("entrandodrop22");
    	$("#dragLateralCompleto").css('display', 'none');
    	$("#lateralDrag").css('width', 20+'%');
    	$("#lateralDrag").css('background', 'none');
	}else{
		console.log("entrandodrop22");
		$("#lateralDrag").css('width', 70+'%');
	}
}

function leaveDrag(ev) {
   console.log("anchoTotal");
   if(anchoTotal*.95<=anchoLateral)
    $("#dragLateralCompleto").css('display', 'none');
}

*/