var latitudAct;
var longitudAct;
var contendermapa;
var contendermapita;
var marker;
var mapita;
var mapitaCreado=0;
function crearMapa(){
    
    var top = document.getElementById("cancelar");
    var contendermapa = document.createElement('div');
    contendermapa.setAttribute('id','contenedormapa');
    contendermapa.setAttribute('class','quincePixeles');
    var mapa = document.createElement('div');
    mapa.setAttribute('id','mapa');
    mapa.setAttribute('class','quincePixeles');
    document.body.insertBefore(contendermapa, top);
    contendermapa.appendChild(mapa);
    L.mapbox.accessToken = 'pk.eyJ1Ijoid2FsbGF2aSIsImEiOiJsOUNmUjdzIn0.tpkK1ucPAVnGuW4rEfLjzg';
    var map = L.mapbox.map('mapa', 'mapbox.streets')
    .setView([latitudAct,longitudAct], 14);
    var crosshairIcon = L.icon({
        iconUrl: 'objetos/negocios/geolocation.png',
        iconSize:     [35, 35], // size of the icon
        iconAnchor:   [15, 15], // point of the icon which will correspond to marker's location
        
    });
    crosshair = new L.marker(map.getCenter(), {icon: crosshairIcon, clickable:false});
    crosshair.addTo(map);

    // Move the crosshair to the center of the map when the user pans
    map.on('move', function(e) {
        crosshair.setLatLng(map.getCenter());
    });

    var botoSeleccionar = document.createElement('div');
    botoSeleccionar.setAttribute('id','botonSelec');
    botoSeleccionar.setAttribute('class','quincePixeles');
    contendermapa.appendChild(botoSeleccionar);

    var botonSelP = document.createElement('p');
    var botonSelText =  document.createTextNode('Seleccionar ubicación');
    botonSelP.setAttribute('id', 'botonSelP');
    botonSelP.setAttribute('class', 'cuarentaPixeles');
    botonSelP.appendChild(botonSelText);
    botoSeleccionar.appendChild(botonSelP); 

    cargado();

    $(document).on('click','#botonSelP',function(){
        //document.body.style.overflow = 'hidden';
        
        latitudActual=map.getCenter().lat;
        longituActual=map.getCenter().lng;
        //console.log(latitudAct);
        ban1=0;
        bandera1(ban1);
        salirMapa();
        
        
    });
}

function salirMapa(){
    $("#contenedormapa").remove();
    if(mapitaCreado==0){
        crearMapaEditar();
    }else{
        marker.setLatLng([latitudActual, longituActual]);
        mapita.setView([latitudActual,longituActual], 15);
    }
    //crearMapaEditar();
    
}

function crearMapaEditar(){
    mapitaCreado=1;
    $('#contenedormapita').css('display', "block");
    $('#sobreponerMapa').css('display', "block");
    var mapitaEsta = document.createElement('mapita');
    //if(longituActual!=undefined && longituActual!="" && latitudActual!=undefined && latitudActual!="" ){
        L.mapbox.accessToken = 'pk.eyJ1Ijoid2FsbGF2aSIsImEiOiJsOUNmUjdzIn0.tpkK1ucPAVnGuW4rEfLjzg';
        mapita = L.mapbox.map('mapita', 'mapbox.streets')
        .setView([latitudActual, longituActual], 15);
    //}  
        marker = L.marker([latitudActual, longituActual], {
            icon: L.icon({
                iconUrl: 'objetos/negocios/geolocation.png',
                iconSize: [35, 35], // size of the icon
                iconAnchor: [15, 15], // point of the icon which will correspond to marker's location
               
            })
        }).addTo(mapita);

        var ancho = $( window ).width();
        $('#contenedormapita').css('height', $('#contenedormapita').width() / 2.25);
        $('#contenedormapita').css('padding-top', ancho*.007);
        $('#contenedormapita').css('padding-bottom', ancho*.036);
        $('#sobreponerMapa').css('height', $('#contenedormapita').width() / 2.15);
        $('#sobreponerMapa').css('bottom', ancho*.19);
        $('#mapita').css('height', 100+"%");
        cargado();
    }

function salirMapita(){
    $("#contenedormapita").remove(); 
}

function crearMapaNegocio(){

        L.mapbox.accessToken = 'pk.eyJ1Ijoid2FsbGF2aSIsImEiOiJsOUNmUjdzIn0.tpkK1ucPAVnGuW4rEfLjzg';
        var mapita = L.mapbox.map('mapa', 'mapbox.streets')
        .setView([latitudActual,longituActual], 15);
        L.marker([latitudActual, longituActual], {
            icon: L.icon({
                iconUrl: 'objetos/negocios/geolocation.png',
                iconSize: [35, 35], // size of the icon
                iconAnchor: [15, 15], // point of the icon which will correspond to marker's location
               
            })
        }).addTo(mapita);

        cargado();
}