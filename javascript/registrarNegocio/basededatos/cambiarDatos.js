//cambiar contraseña
function cambiarContrasena(){
	var passActual = $("#contrasenaActual").val();
    var password = $("#nuevaContrasena").val();
    var passwordRep = $("#repetirNuevaContrasena").val();

        if(passActual){
            if(password){
                if(password!=passwordRep){
                	notificaciones('Tidingo', 'las contraseñas son diferentes');
                }else{
                	 firebase.auth().signInWithEmailAndPassword(email, passActual).then(function(user){
	                    //alert("Usuario ingresado con exito");

						user.updatePassword(password).then(function() {
							var user = firebase.auth().currentUser;
                			userId = user.uid;
                			palabr="Tu correo ha sido modificado";
                			contraChange(palabr);
						  	alert("Tu contraseña ha sido modificada");
						  	location.href="negocio.html?"+userId;

						}, function(error) {
						  	var errorCode = error.code;
	                    	var errorMessage = error.message;
	                    	if(errorCode=="auth/invalid-email"){
	                        notificaciones('Tidingo', 'El correo no es valido');
	                        return false;
	                    
		                    }else if(errorCode=="auth/wrong-password"){
		                        notificaciones('Tidingo', 'La contraseña no es valida'); 
		                        return false;
		                    
		                    }else if(errorCode=="auth/network-request-failed"){

		                    }else if(errorCode=="auth/user-not-found"){
		                        notificaciones('Tidingo', 'Este correo no corresponde a ningun usuarios');
		                        return false;
		                    }else if(errorCode=="auth/weak-password"){
	                            notificaciones('Tidingo', 'Escribe al menos seis caracteres');
	                            return false;
	                        }else{
		                        
		                        alert(errorCode);
		                        notificaciones('Tidingo', errorMessage);
		                        return false;
		                    }
						});
	                    // 
	                }).catch(function(error) {
	                
	                  // Handle Errors here.
	                    var errorCode = error.code;
	                    var errorMessage = error.message;

	                    if(errorCode=="auth/invalid-email"){
	                        notificaciones('Tidingo', 'El correo no es valido');
	                        return false;
	                    
	                    }else if(errorCode=="auth/wrong-password"){
	                        notificaciones('Tidingo', 'La contraseña no es valida'); 
	                        return false;
	                    
	                    }else if(errorCode=="auth/network-request-failed"){

	                    }else if(errorCode=="auth/user-not-found"){
	                        notificaciones('Tidingo', 'Este correo no corresponde a ningun usuarios');
	                        return false;
	                    }else if(errorCode=="auth/weak-password"){
                            notificaciones('Tidingo', 'Escribe al menos seis caracteres');
                            return false;
                        }else{
	                        
	                        alert(errorCode);
	                        notificaciones('Tidingo', errorMessage);
	                        return false;
	                    }
	                    
	                                   
	                  // ...
	                });
                	
                }
            }else{
            	notificaciones('Tidingo', 'Ingresa la contraseña nueva');
            }
    }else{
    	notificaciones('Tidingo', 'Ingresa la contraseña actual');
    }
}

//cambiar Email
function cambiarEmail(){
	var nuevoCorreo = $("#nuevoCorreo").val();
    var password = $("#contrasenaConfirmacion").val();

        if(nuevoCorreo){
            if(password){
                	 firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){

						user.updateEmail(nuevoCorreo).then(function() {
						  // Update successful.
						  	var user = firebase.auth().currentUser;
                			userId = user.uid;
						  	palabr="Tu correo ha sido modificado";
                			contraChange(palabr);
							location.href="negocio.html?"+userId;

						}, function(error) {
							var errorCode = error.code;
	                    	var errorMessage = error.message;

						  	if(errorCode=="auth/invalid-email"){
	                        notificaciones('Tidingo', 'El correo no es valido');
	                        return false;
	                    
		                    }else if(errorCode=="auth/wrong-password"){
		                        notificaciones('Tidingo', 'La contraseña no es valida'); 
		                        return false;
		                    
		                    }else if(errorCode=="auth/network-request-failed"){

		                    }else if(errorCode=="auth/user-not-found"){
		                        notificaciones('Tidingo', 'Este correo no corresponde a ningun usuarios');
		                        return false;
		                    }else if(errorCode=="auth/weak-password"){
	                            notificaciones('Tidingo', 'Escribe al menos seis caracteres');
	                            return false;
	                        }else{
		                        
		                        alert(errorCode);
		                        notificaciones('Tidingo', errorMessage);
		                        return false;
		                    }
						});
	                    // 
	                }).catch(function(error) {
	                
	                  // Handle Errors here.
	                    var errorCode = error.code;
	                    var errorMessage = error.message;

	                    if(errorCode=="auth/invalid-email"){
	                        notificaciones('Tidingo', 'El correo no es valido');
	                        return false;
	                    
	                    }else if(errorCode=="auth/wrong-password"){
	                        notificaciones('Tidingo', 'La contraseña no es valida'); 
	                        return false;
	                    
	                    }else if(errorCode=="auth/network-request-failed"){

	                    }else if(errorCode=="auth/user-not-found"){
	                        notificaciones('Tidingo', 'Este correo no corresponde a ningun usuario');
	                        return false;
	                    }else if(errorCode=="auth/weak-password"){
                            notificaciones('Tidingo', 'Escribe al menos seis caracteres');
                            return false;
                        }else{
	                        
	                        alert(errorCode);
	                        notificaciones('Tidingo', errorMessage);
	                        return false;
	                    }
	                    
	                                   
	                  // ...
	                });
                	
            }else{
            	notificaciones('Tidingo', 'Ingresa la contraseña');
            }
    }else{
    	notificaciones('Tidingo', 'Ingresa tu correo nuevo');
    }
}