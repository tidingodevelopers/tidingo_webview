    
function registrarNeg(){
    var email = $("#correoRegistrarNegocio").val();
    var password = $("#passwordRegistrarNegocio").val();
    var passwordRep = $("#repetirPasswordRegistrarNegocio").val();
    var codigoNeg = $("#codigoRegistroNegocio").val();

    queryCodigos = firebase.database().ref('codigos').orderByKey().equalTo(codigoNeg);
    queryCodigos.once('value').then(function(snapshot) {
        var codigo = snapshot.val();
        if(codigo!=null){
            if(email){
                if(password){
                    if(password!=passwordRep){
                        notificaciones('Tidingo', 'las contraseñas son diferentes');
                        return false;
                    }else{
                        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
                            writeUserData(email,codigoNeg);
                            function writeUserData(email,codigoNeg) {
                                
                                var user = firebase.auth().currentUser;
                                userId = user.uid;
                              firebase.database().ref('users/' + userId).set({
                                ident : userId,
                                correo : email,
                                nombre : "",
                                descripcion : "",
                                pagweb : "",
                                facebook : "",
                                telefono : "",
                                horario : "",
                                calle : "",
                                numero : "",
                                colonia : "",
                                municipio : "",
                                estado : "",
                                palabrasc : "",
                                codigo: codigoNeg
                              }).then(function(){
                                cargandoPaginas();
                                location.href="editarNegocio.html?"+userId;
                              });
                              
                            }
                            
                            return false;
                        }).catch(function(error) {
                        
                          // Handle Errors here.
                        var errorCode = error.code;
                        var errorMessage = error.message;
                            if(errorCode=="auth/network-request-failed"){
                            
                            }else if(errorCode=="auth/email-already-in-use"){
                                notificaciones('Tidingo', 'Este usuario ya esta en uso');
                                return false;
                                
                            }else if(errorCode=="auth/invalid-email"){
                                notificaciones('Tidingo', 'El correo no es valido');
                                return false;
                            }else if(errorCode=="auth/weak-password"){
                                notificaciones('Tidingo', 'Escribe al menos seis caracteres');
                                return false;
                            }else{
                                alert(errorCode);
                                notificaciones('Tidingo', errorMessage);
                                return false;
                            }
                                           
                          // ...
                        });

                    }   
                }else{
                    notificaciones('Tidingo', 'Ingresa la contraseña');
                    return false;

                }
            }else{
                notificaciones('Tidingo', 'Ingresa tu correo electronico');
                return false;
            }
        }else{
            notificaciones('Tidingo', 'Código equivocado');
        }

    });

}


function iniciarSes(){    
    var email = $("#correoIniciarSecion").val();
    var password = $("#passwordIniciarSesion").val();

    if(email){
        if(password){
            firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){
                var user = firebase.auth().currentUser;
                userId = user.uid;
                cargandoPaginas();
                location.href="negocio.html?"+userId;

            }).catch(function(error) {
            
              // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;

                if(errorCode=="auth/invalid-email"){
                    notificaciones('Tidingo', 'El Correo no es valido');
                    return false;
                
                }else if(errorCode=="auth/wrong-password"){
                    notificaciones('Tidingo', 'La contraseña no es valida'); 
                    return false;
                
                }else if(errorCode=="auth/network-request-failed"){

                }else if(errorCode=="auth/user-not-found"){
                    notificaciones('Tidingo', 'Este correo no corresponde a ningun usuario');
                    return false;
                }else{
                    
                    alert(errorCode);
                    notificaciones('Tidingo', errorMessage);
                    return false;
                }
                
                               
              // ...
            });
   
        }else{
            notificaciones('Tidingo', 'Ingresa la contraseña');
            return false;

        }
    }else{
        notificaciones('Tidingo', 'Ingresa tu correo electronico');
        return false;
    }
}


